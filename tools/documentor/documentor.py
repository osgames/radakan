#!/usr/bin/env python
# -*- coding: utf-8 -*-

from subprocess import call

def execute():

    print 'Documenting...'

    call('epydoc --html --output=doc -v -v -v --inheritance grouped --name=Radakan --url=http://www.radakan.org/ --no-frames --no-sourcecode --exclude=reactio --exclude=rwe --graph all client/ radakan.py common/ tools/'.split())

    print u'The documentation can be found under \'doc\'.'

    return 0
