#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import defaultTestLoader, TestCase, TestSuite, TextTestRunner

from common.percentage import percentage

from client.core.skills.skill_level import Skill_Level

class Skill_Level_Test(TestCase):

    def setUp(self):

        # giftedness, experience, skillfulness, float
        configurations = ((1, 0, 1, 1), (0.7, 2, 0.714, 0.8331389), (1.5, 5, 1.575, 1.223301), (19, 1234, 253.46, 1.9921402))

        self.skill_levels = dict()

        for configuration in configurations:
            self.skill_levels[configuration] = Skill_Level(configuration[0])
            self.skill_levels[configuration].add_experience(configuration[1])

    def test_giftedness(self):
        for configuration, skill_level in self.skill_levels.iteritems():
            self.assertAlmostEqual(skill_level.giftedness, configuration[0])

    def test_expierience(self):
        for configuration, skill_level in self.skill_levels.iteritems():
            self.assertAlmostEqual(skill_level.experience, 1 + 0.01 * configuration[1])

    def test_skillfulness(self):
        for configuration, skill_level in self.skill_levels.iteritems():
            self.assertAlmostEqual(skill_level.skillfulness, configuration[2])

    def test_float(self):
        for configuration, skill_level in self.skill_levels.iteritems():
            self.assertAlmostEqual(float(skill_level), configuration[3])

def execute():

    test_suite = TestSuite()
    test_suite.addTest(defaultTestLoader.loadTestsFromTestCase(Skill_Level_Test))

    TextTestRunner(verbosity=2).run(test_suite)
