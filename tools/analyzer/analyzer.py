#!/usr/bin/env python
# -*- coding: utf-8 -*-

from glob import glob
from os import chdir, rename
from os.path import normpath
from sys import argv, exit as exit_

from pylint.lint import Run as run_pylint

def execute():

    print 'Analyzing...'

    output_directory = normpath('logs/analyzer')
    chdir(output_directory)

    # Dirty job to close pylint left opened
    import __builtin__
    original_open = __builtin__.open
    f_handlers = []
    def safe_open(*arg, **kw):
        f = original_open(*arg, **kw)
        f_handlers.append(f)
        return f
    __builtin__.open = safe_open

    # These are relative paths from the output directory.
    try:
        run_pylint(['--rcfile=../../tools/analyzer/analyzer.cfg', '../../common', '../../client/core', '../../radakan.py'])
        # Adding '../../tools' results in a segmentation error in pylint.
    except SystemExit, system_exit:
        print u'Prevented a %s: %s' % (type(system_exit), system_exit)
    except BaseException, exception:
        print u'%s: %s' % (type(exception), exception)
        raise exception

    # Restore the original file open
    __builtin__.open = original_open

    for f in f_handlers:
        f.close()

    # TODO: handle exceptions.
    f_all = open('all.txt', 'w')
    f_some = open('some.txt', 'w')

    # Extrace from pylint_global.txt
    f_global = open('pylint_global.txt', 'r')
    global_lines = f_global.readlines()
    f_global.close()

    errors = 1
    for lineno in range(len(global_lines)):
        line = global_lines[lineno]
        if 0 <= line.find('Messages by category'):
            f_all.writelines(global_lines[lineno:lineno+15])
            f_some.writelines(global_lines[lineno:lineno+15])

        if line.startswith('|error'):
            errors = int(global_lines[lineno].split('|')[2])
            if errors == 0:
                print 'No errors were detected.'
            elif errors == 1:
                print '1 error was detected.'
            else:
                print '%d error(s) were detected.' % errors

        if 0 <= line.find('Your code has been rated at'):
            print 'Code rating: %3.1f %%.' % (float(line.replace('Your code has been rated at ', '').split('/')[0]) * 10)

    # Append all files into all.txt and filter C0111 into some.txt  except global.txt and rename theme
    file_list = glob('pylint_*.txt')
    file_list.sort()
    for file_name in file_list:
        if file_name != 'pylint_global.txt':
            f = open(file_name, 'r')
            f_lines = f.readlines()
            f.close()

            # Append all content to all.txt
            f_all.writelines(f_lines)
            # Filter some.txt
            f_lines = [line for line in f_lines if (line.find('C0111') < 0) and (line.find('F') != 0)]
            if 0 < len(f_lines):
                f_some.writelines(f_lines)

        rename(file_name, file_name.replace('pylint_', '', 1))

    f_some.close()
    f_all.close()

    print u'See \'%s/some.txt\' for a list of the most important problems.' % output_directory

    chdir(normpath('../..'))

    return 1 if errors else 0
