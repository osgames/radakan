# -*- coding: utf-8 -*-

'''
This module defines the L{percentage} function.
'''

def percentage(value):

    # 'value' could be an instance of a type with a '__float__' method.
    return u'%.0f %%' % (100 * float(value))
