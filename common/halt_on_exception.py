# -*- coding: utf-8 -*-
'''
This module defines the 'halt_on_exception' decorator.
'''

try:
    from decorator import decorator
except ImportError, error:
    raise ImportError, 'The \'decorator\' module could not be found. Did you install it?\n%s' % error

@decorator
def halt_on_exception(function, * args, ** kwargs):
    '''
    This method is a gentler approach than 'abort_on_exception'. In certain cases, like inside a __del__ method, it doesn't suffice to halt the program.
    '''

    try:
        return function(* args, ** kwargs)
    except Exception, exception:

        print u'*** HALT ***'
        print u''
        print u'%s in %s: %s' % (type(exception).__name__, function, unicode(exception))
        print u''

        from common.get_call_string import get_call_string
        from common.print_exception_details import print_exception_details
        print_exception_details(exception, get_call_string(function, * args, ** kwargs))

        raise SystemExit
