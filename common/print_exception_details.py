# -*- coding: utf-8 -*-

from traceback import format_exc, format_stack

def print_exception_details(exception, where):

    print u'*** A %s occured in \'%s\': %s ***' % (type(exception).__name__, where, exception)
    print u''
    print u'Exception stack:'
    print u''.join(format_stack())

    print format_exc(50)
