# -*- coding: utf-8 -*-

def get_call_string(function, * args, ** kwargs):

    arguments = list()

    for arg in args:
        try:
            arguments.append(u'\'%s\'' % arg)
        except TypeError, type_error:
            # lazy import to prevent an ImportError

            # In some cases the __repr__ method of a Panda3D object returns 'None'...
            # The Panda3D people should be informed when this bug occurs.
            print u'Panda3D bug: deliberately ignored %s: %s' % (type(type_error).__name__, type_error)
            arguments.append(u'\'%s\'' % object.__repr__(arg))

    for pair in kwargs.iteritems():
        arguments.append(u'%s=\'%s\'' % pair)

    function_name = function.func_name

    try:
        for cls in type(args[0]).__mro__:
            if cls.__dict__.get(function_name) != None:
                function_name = u'%s.%s' % (args[0], function_name)
                arguments = arguments[1:]
                break
    except (AttributeError, IndexError, TypeError):
        pass

    return u'%s(%s)' % (function_name, u', '.join(arguments))
