# -*- coding: utf-8 -*-

from ConfigParser import NoOptionError

from client.core.configuration import get_configuration, set_configuration
from client.core.events import configuration_option_change

class Configuration_Option(object):

    def __init__(self, section, name, default, test=lambda a: True, modifier=lambda a: a):

        self.__section = section
        self.__name = name
        self.__type = type(default)
        self.__test = test
        self.__modifier = modifier

        # We'll use a cached value instead of looking it up repeatedly to speed things up.
        try:
            self.raw_value = get_configuration(self.__section, self.__name)
        except (NoOptionError, ValueError):
            self.__raw_value = default

    @property
    def raw_value(self):

        return self.__raw_value

    @raw_value.setter
    def raw_value(self, new_value):

        if self.__type == bool:

            new_value = 't' in str(new_value).lower()

        else:

            new_value = self.__type(new_value)

        if not self.__test(new_value):
            print u'\'%s\' doesn\'t match for option \'%s\' in section \'%s\'.' % (new_value, self.__name, self.__section)
            raise ValueError

        self.__raw_value = new_value

        set_configuration(self.__section, self.__name, str(self.__raw_value))

        if messenger != None:
            messenger.send(configuration_option_change, [self])

    @property
    def value(self):

        return self.__modifier(self.__raw_value)
