# -*- coding: utf-8 -*-

from client.core.configuration_options import point_of_view

def __is_player_character(noun):

    from client.core.items.player_character import Player_Character
    return unicode(noun) == unicode(player_character)

def capitalize(noun, pc=False):

    if (pc and point_of_view.value == 3) or __is_player_character(noun):
        # Do not capitalize the player character name. Most players don't like it.
        return unicode(noun)

    noun = unicode(noun)

    return noun[0].upper() + noun[1:]

def conjugate(verb, noun, pov=None, pc=False):

    if pc or __is_player_character(noun):

        if pov == None:
            pov = point_of_view.value

        if pov < 3:
            return verb

    if verb[- 1] in ('h', 's'):
        return u'%ses' % verb
    else:
        return u'%ss' % verb

def object_(noun, pc=False):

    if pc or __is_player_character(noun):

        if point_of_view.value == 1:
            return u'me'
        elif point_of_view.value == 2:
            return u'you'

    return unicode(noun)

def possessive(noun, pov=None, pc=False):

    if pc or __is_player_character(noun):

        if pov == None:
            pov = point_of_view.value

        if pov == 1:
            return u'my'
        elif pov == 2:
            return u'your'

    noun = unicode(noun)

    if noun[-1] in u'sz':
        return u'%s\'' % noun
    else:
        return u'%s\'s' % noun

def subject(noun, pov=None, pc=False):

    if pc or __is_player_character(noun):

        if pov == None:
            pov = point_of_view.value

        if pov == 1:
            return u'I'
        elif pov == 2:
            return u'you'

    return unicode(noun)
