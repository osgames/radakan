# -*- coding: utf-8 -*-

'''
This defines the L{Race} class.
'''

class Race(object):

    def __init__(self, name, description, genders):

        super(Race, self).__init__()

        self.__name = unicode(name)
        self.__description = unicode(description)
        self.__genders = genders

    def __unicode__(self):

        return self.__name

    @property
    def description(self):

        return self.__description

    @property
    def genders(self):

        return self.__genders
