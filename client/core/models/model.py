# -*- coding: utf-8 -*-

'''
This defines the L{Model} class.
'''

from math import atan2, degrees

try:
    from direct.actor.Actor import Actor
    from panda3d.core import BitMask32, CollisionNode, CollisionHandlerFloor, CollisionHandlerPusher, CollisionRay, CollisionTraverser, NodePath, PandaNode, Point2, Point3
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from common.halt_on_exception import halt_on_exception

from client.core.configuration_options import gui_scale
from client.core.models.model_blueprint import Model_Blueprint
from client.core.observer import Observer
from client.core.vector3 import Vector3

# target selection picking:
# - from ray into some meshes
# - target_selection_mask
# - CollisionHandlerQueue

# goal setting:
# - from ray into all meshes
# - goal_mask
# - CollisionHandlerQueue

# horizontal collision:
# - from some solids into solids
# - wall_mask
# - CollisionHandlerPusher

# vertical collision:
# - from items into terrain
# - floor_mask
# - CollisionHandlerFloor

empty_mask = BitMask32(0)
target_selection_mask = BitMask32(1)
goal_mask = BitMask32(1)
floor_mask = BitMask32(4)
wall_mask = BitMask32(8)

_floor_collision_handler = None
_wall_collision_handler = None

class _Collision_Activator(Observer):

    def __init__(self):

        super(_Collision_Activator, self).__init__()

        self.accept('solids-into-solids', self.__activate_collided_solid)

    def __activate_collided_solid(self, collision_event):

        item = collision_event.getIntoNodePath().getNetPythonTag('item')

        if item != None:

            item.model.start_collision()

__collision_activator = _Collision_Activator()

def get_new_default_model(icon='back'):

    return Model(blueprint=Model_Blueprint(name=u'bowl_test_02', icon=icon), coordinates=Vector3(0, 0, 0))

def prepare_collision_handling():

    base.cTrav = CollisionTraverser()

    # Make sure we can catch all fast moving objects.
    base.cTrav.setRespectPrevTransform(True)

    #base.cTrav.showCollisions(render) # for debugging

    global _floor_collision_handler
    _floor_collision_handler = CollisionHandlerFloor()
    _floor_collision_handler.setMaxVelocity(10)

    global _wall_collision_handler
    _wall_collision_handler = CollisionHandlerPusher()
    _wall_collision_handler.addInPattern('solids-into-solids')

class Model(NodePath):

    def __init__(self, blueprint, coordinates, group=None, horizontal_angle=0, scale=1):

        super(Model, self).__init__(PandaNode('groud node'))

        self.__detecting_collision = False

        mesh_node = None
        if 0 < len(blueprint.animations):
            mesh_node = Actor(blueprint.path, blueprint.animations)
        else:
            mesh_node = loader.loadModel(blueprint.path)

        self.__mesh_node_path = NodePath(mesh_node)
        if blueprint.terrain:
            self.__mesh_node_path.setCollideMask(floor_mask | goal_mask)
        else:
            self.__mesh_node_path.setCollideMask(goal_mask)
        self.__mesh_node_path.setPos(Vector3(0, 0, blueprint.height_offset))
        self.__mesh_node_path.setScale(blueprint.scale * scale)
        self.__mesh_node_path.setHpr(* blueprint.orientation)
        self.__mesh_node_path.reparentTo(self)

        # Stored so the model can be saved later.
        self.__blueprint = blueprint

        if coordinates != None:
            # Do not create a 'Displacement' event during world creation.
            self.setPos(coordinates)

        # Do not create a 'Rotation' event during world creation. Event observers might not be ready yet.
        self.setH(horizontal_angle)

        if group == None:
            self.appear()
        else:
            self.reparentTo(group)

        self.__collision_node_path = None
        self.__gravity_node_path = None

        if blueprint.collision_solids_generator != None:

            # We'll use the more advanced wall collision instead of floor collision.
            self.__mesh_node_path.setCollideMask(empty_mask)

            self.__collision_node_path = self.attachNewNode(CollisionNode('collision node for %s' % blueprint.path))

            for collision_solid in blueprint.collision_solids_generator():
                self.__collision_node_path.node().addSolid(collision_solid)

            self.__collision_node_path.node().setFromCollideMask(empty_mask)
            self.__collision_node_path.node().setIntoCollideMask(wall_mask)
            #self.__collision_node_path.show() # for debugging

            _wall_collision_handler.addCollider(self.__collision_node_path, self)

    @property
    @halt_on_exception
    def center_2d(self):

        # Get the 3D center, relative to 'render'.
        center = self.coordinates + self.__mesh_node_path.getBounds().getCenter()

        # Get the 3D center, relative to 'base.cam'.
        center = base.cam.getRelativePoint(render, center)

        projected = Point2()

        # Get the projected 2D center.
        base.camLens.project(center, projected)

        # Get the projected 2D center in 'render2d' scale.
        projected = aspect2d.getRelativePoint(render2d, Point3(projected[0], 0, projected[1]))

        # Return the projected 2D center in HUD scale.
        return projected[0] / gui_scale.value, projected[2] / gui_scale.value

    @property
    def coordinates(self):

        return Vector3(self.getPos())

    @coordinates.setter
    @halt_on_exception
    def coordinates(self, new_coordinates):

        self.setFluidPos(* new_coordinates.coordinates)

        self.start_collision()

    @property
    @halt_on_exception
    def forward(self):

        backward = self.getNetTransform().getMat().getRow3(1)
        backward.setZ(0)
        backward.normalize()
        return - Vector3(backward)

    @property
    def highlighting(self):

        return None # TODO

    @highlighting.setter
    @halt_on_exception
    def highlighting(self, light):

        self.clearLight()

        if light != None:
            self.setLight(light)

    @property
    def horizontal_angle(self):

        return self.getH()

    @horizontal_angle.setter
    @halt_on_exception
    def horizontal_angle(self, angle):

        self.setH(angle)

    @property
    def icon_path(self):

        return self.__blueprint.icon_path

    @property
    def item(self):

        return self.getNetPythonTag('item')

    @item.setter
    @halt_on_exception
    def item(self, new_item):

        assert self.item == None, u'The item of this model has already been set.'

        self.setPythonTag('item', new_item)

        if new_item != None:

            # The item will call 'appear()' on me when appropriate.
            self.disappear()

            # Allow incomming collisions for the mouse picker, but leave the existing masks intact.
            # 'setCollideMask' only affects the 'into' mask.
            self.__mesh_node_path.setCollideMask(self.__mesh_node_path.getCollideMask() | target_selection_mask)

            if self.item.weight < float('inf'):

                # Detect outgoing floor collisions.
                self.__gravity_node_path = self.attachNewNode(CollisionNode('gravity'))
                self.__gravity_node_path.node().addSolid(CollisionRay(0, 0, 0.5, 0, 0, - 1))
                self.__gravity_node_path.node().setFromCollideMask(floor_mask)
                self.__gravity_node_path.node().setIntoCollideMask(empty_mask)
                #self.__gravity_node_path.show() # for debugging

                _floor_collision_handler.addCollider(self.__gravity_node_path, self)

                if self.__collision_node_path != None:

                    # Detect outgoing wall collisions.
                    self.__collision_node_path.node().setFromCollideMask(wall_mask)

    @property
    def radius(self):

        return self.__blueprint.radius

    @property
    def visible(self):

        return not self.isHidden()

    def appear(self):

        self.reparentTo(render)

    def disappear(self):

        self.detachNode()

    def fall(self):
        '''This method is intended for character that fall on the fround.'''

        self.__mesh_node_path.setFluidPos(self.coordinates + Vector3(0, 0, 0.2))
        self.__mesh_node_path.setP(60)
        self.__mesh_node_path.setR(- 90)

    def get_distance(self, other):

        return self.coordinates.get_distance(other)

    def get_horizontal_angle(self, other):
        '''This angle might not be normalized.'''

        target_direction = other.coordinates - self.coordinates

        return degrees(atan2(target_direction.x, target_direction.y)) - degrees(atan2(self.forward.x, self.forward.y))

    def get_save_data(self):

        return u'Model(coordinates={self.coordinates}, offset={self._Model__offset}, name={self._Model__model_blueprint.name}, animations={self._Model__model_blueprint.animations}, scale={self._Model__model_blueprint.scale}, hpr={hpr}, visible={self.visible})'.format(hpr=self.getHpr(), ** vars())

    def start_collision(self):

        if  (self.item.weight < float('inf')) and not self.__detecting_collision:

            self.__detecting_collision = True

            base.cTrav.addCollider(self.__gravity_node_path, _floor_collision_handler)

            if self.__collision_node_path != None:
                base.cTrav.addCollider(self.__collision_node_path, _wall_collision_handler)

            # Call '__stop_collision' in 8 seconds.
            taskMgr.doMethodLater(8, self.__stop_collision, 'Stop collision')

    def __stop_collision(self, not_used):

        base.cTrav.removeCollider(self.__gravity_node_path)

        if self.__collision_node_path != None:
            base.cTrav.removeCollider(self.__collision_node_path)

        # Make sure collision detection can be started once we move again.
        self.__detecting_collision = False
