# -*- coding: utf-8 -*-

try:
    from panda3d.core import RigidBodyCombiner, NodePath
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

class Model_Group(NodePath):

    def __init__(self, name):

        super(Model_Group, self).__init__(RigidBodyCombiner(name))

        self.reparentTo(render)

    def collect(self):

        self.node().collect()
