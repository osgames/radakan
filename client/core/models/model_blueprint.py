# -*- coding: utf-8 -*-

'''
This defines the L{Model_Blueprint} class.
'''

_art_base_path = u'client/art'

def set_app_runner_model_path(app_runner_path):

    global _art_base_path
    _art_base_path = u'%s/%s' % (app_runner_path, _art_base_path)

class Model_Blueprint(object):

    def __init__(self, name, animations={}, collision_solids_generator=None, height_offset=0, icon='back', orientation=(0, 0, 0), radius=0, scale=1, terrain=False):

        super(Model_Blueprint, self).__init__()

        self.__animations = animations
        self.__collision_solids_generator = collision_solids_generator
        self.__height_offset = height_offset
        self.__icon_path = u'%s/icons/%s.png' % (_art_base_path, icon)

        # We use the file path *without* the extension. Panda3D automatically adds it.
        # When bluiding the .egg files are converted to .bam files, so the extension may vary.
        self.__path = u'%s/models/%s' % (_art_base_path, name)

        self.__orientation = orientation
        self.__radius = radius
        self.__scale = scale
        self.__terrain = terrain

    @property
    def animations(self):

        return self.__animations

    @property
    def collision_solids_generator(self):

        return self.__collision_solids_generator

    @property
    def height_offset(self):

        return self.__height_offset

    @property
    def icon_path(self):

        return self.__icon_path

    @property
    def path(self):

        return self.__path

    @property
    def orientation(self):

        return self.__orientation

    @property
    def scale(self):

        return self.__scale

    @property
    def radius(self):

        return self.__radius

    @property
    def terrain(self):

        return self.__terrain
