# -*- coding: utf-8 -*-

from client.core.configuration_option import Configuration_Option

__user_interface = u'user interface'

#fullscreen = u'fullscreen' # TODO

# Use the modifier to make sure a 2 units wide widget still fits in the screen.
gui_scale = Configuration_Option(__user_interface, u'gui_scale', 0.7, test=lambda a: 0.4 <= a <= 1.3, modifier=lambda a: min(a, base.a2dRight))
gui_scale.range = (0.4, 1.3)

play_music = Configuration_Option(__user_interface, u'play_music', True)
music_volume = Configuration_Option(__user_interface, u'music_volum', 0.5, test=lambda a: 0 <= a <= 1)

point_of_view = Configuration_Option(__user_interface, u'point_of_view', 1, test=lambda a: a in range(1, 4))

show_frame_rate = Configuration_Option(__user_interface, u'show_frame_rate', False)

window_left = Configuration_Option(__user_interface, u'window_left', 50)
window_top = Configuration_Option(__user_interface, u'window_top', 50)

window_width = Configuration_Option(__user_interface, u'window_width', 900, test=lambda a: 0 < a)
window_height = Configuration_Option(__user_interface, u'window_height', 650, test=lambda a: 0 < a)
