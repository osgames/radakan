# -*- coding: utf-8 -*-

from client.core.events import journey_state_change, tracker_change
from client.core.observer import Observer

class Tracker(Observer):

    def __init__(self, name, value):

        self.__name = name
        self.__initial_value = value
        self.__value = value

        self.accept(journey_state_change, self.__handle_journey_state_change)

    @property
    def name(self):

        return self.__name

    @property
    def value(self):

        return self.__value

    @value.setter
    def value(self, value):

        self.__value = value

        messenger.send(tracker_change, [self])

    def __handle_journey_state_change(self, state):

        if state == journey_state_change.end:

            self.__value = self.__initial_value

deaths_caused = Tracker(u'Deaths caused', 0)
quests_completed = Tracker(u'Quests completed', 0)

trackers = (deaths_caused, quests_completed)