# -*- coding: utf-8 -*-

from client.core.enum import Enum

condition_change = u'condition change' # argument: item
configuration_option_change = u'configuration option change' # argument: configuration_option
hovered_item_change = u'hovered item change' # argument: item
mouse_over_frame_change = u'mouse over frame change' # argument: mouse_over_frame
mouse_view_change = u'mouse view change' # argument: mouse_view
player_character_action_change = u'player character action change'
player_character_perception = u'player character perception' # argument: perception
player_character_target_change = u'player character target change'
proceeding = u'proceeding' # argument: action
recontainment = u'recontainment' # arguments: item, source
tracker_change = u'tracker_change' # argument: tracker
world_update_exception = u'world update exception' # argument: exception

#displacement = u'displacement'
#rotation = u'rotation'
#transformation = u'transformation'

journey_state_change = Enum(('run', 'pause', 'player_character_death', 'end'))
# 'run' is used for both player character creation and resuming after a pause.

screen_change = Enum(('configuration_screen', 'credits_screen', 'head_up_display', 'loading_screen', 'menu_screen', 'player_character_creation_screen', 'player_character_death_screen', 'saving_screen'))
