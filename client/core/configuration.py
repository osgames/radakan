# -*- coding: utf-8 -*-

from os import mkdir

from ConfigParser import DuplicateSectionError, RawConfigParser

__configuration_file_name = u'client/client.cfg'

__parser = RawConfigParser()

def __enter(my_file):

    return my_file

def __exit(my_file, * args):

    my_file.flush()
    my_file.close()

def get_configuration(section, option):

    try:
        __parser.add_section(section)
    except DuplicateSectionError:
        pass

    return __parser.get(section, option).lower()

def set_configuration(section, option, value):

    try:
        __parser.add_section(section)
    except DuplicateSectionError:
        pass

    __parser.set(section, option, value)

    try:
        mkdir('client')
    except OSError:
        pass

    # This is a workaround for the Panda3D-1.7.0 implementation of 'file',
    # which lacks '__enter__' and '__exit__' methods.
    if not '__enter__' in dir(file):

        file.__enter__ = __enter
        file.__exit__ = __exit

    # Make sure we have a configuration file with sane settings.
    with open(__configuration_file_name, 'w') as configuration_file:
        __parser.write(configuration_file)

# This also works if the file doesn't exist.
__parser.read(__configuration_file_name)
# Note that the read settings may be incomplete.
