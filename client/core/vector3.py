# -*- coding: utf-8 -*-

'''
This defines the L{Vector3} class.
'''

from math import sqrt

try:
    from panda3d.core import Vec3
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

class Vector3(Vec3, object):

    def __init__(self, * args):

        Vec3.__init__(self, * args)
        object.__init__(self)

    def __unicode__(self):

        return u'(%.1f, %.1f, %.1f)' % (self.x, self.y, self.z)

    def __add__(self, other):

        try:
            return Vector3(super(Vector3, self).__add__(other))
        except TypeError:
            return Vector3(super(Vector3, self).__add__(other.coordinates))

    def __sub__(self, other):

        try:
            return Vector3(super(Vector3, self).__sub__(other))
        except TypeError:
            return Vector3(super(Vector3, self).__sub__(other.coordinates))

    def __neg__(self):

        return Vector3(super(Vector3, self).__neg__())

    def __mul__(self, factor):

        return Vector3(super(Vector3, self).__mul__(factor))

    def __div__(self, divisor):

        return Vector3(super(Vector3, self).__div__(divisor))

    @property
    def coordinates(self):

        return self

    @property
    def length(self):

        return sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2)

    @property
    def normalized(self):

        return self / self.length

    @property
    def x(self):

        return self.getX()

    @x.setter
    def x(self, other):

        self.setX(new_x)

    @property
    def y(self):

        return self.getY()

    @y.setter
    def y(self, new_y):

        self.setY(new_y)

    @property
    def z(self):

        return self.getZ()

    @z.setter
    def z(self, new_z):

        self.setZ(new_z)

    def get_direction(self, other):

        return (self - other).normalized

    def get_distance(self, other):

        return (self - other).length
