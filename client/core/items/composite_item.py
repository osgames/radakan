# -*- coding: utf-8 -*-

'''
This defines the L{Composite_Item} class.
'''

from random import random

from common.halt_on_exception import halt_on_exception

from client.core.items.container import Container

epsilon = 0.000001

class Composite_Item(Container):

    def __init__(self, ** kwargs):

        super(Composite_Item, self).__init__(capacity=float('inf'), size=None, weight=0, ** kwargs)

    @property
    @halt_on_exception
    def condition(self):

        return min(component.condition for component in self)

    @condition.setter
    @halt_on_exception
    def condition(self, new_condition):
        '''Pick a weighted random component to apply the damage to.'''

        if ((self.condition <= 0) and (new_condition <= 0)) or ((1 <= self.condition) and (1 <= new_condition)):
            # The condition of this item won't become even worse/better.
            return

        component = self.__random_component

        component_condition_change = (new_condition - self.condition) * self.weight / component.weight

        component_new_condition = component.condition + component_condition_change

        # The component will limit the new condition between 0 and 1.
        component.condition = component_new_condition

        if component.condition != component_new_condition:

            Composite_Item.condition.fset(self, self.condition + (component_new_condition - component.condition) * component.weight / self.weight)

    @property
    @halt_on_exception
    def size(self):
        '''The sum of the component sizes.'''

        return sum(component.size for component in self)

    @property
    @halt_on_exception
    def __random_component(self):

        criterion = self.weight * random()

        for component in self:

            criterion -= component.weight

            # Because of floating point operation rounding, we have to compare
            # with a value slightly higher than 0.
            if criterion <= epsilon:
                return component
