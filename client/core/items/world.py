# -*- coding: utf-8 -*-

'''
This defines the L{World} class.
'''

from client.core.items.container import Container

class World(Container):
    '''
    This is the root for the L{Container} tree.
    '''

    def __init__(self):

        super(World, self).__init__(capacity=float('inf'), container=None, name=u'Radakan')

    @property
    def model_item(self):

        return None

    @property
    def owner(self):

        return None

    def add(self, item):

        Container.add(self, item)

        item.model.appear()

    def remove(self, item):

        item.model.disappear()

        Container.remove(self, item)
