# -*- coding: utf-8 -*-

'''
This defines the L{Body_Part} class.
'''
from collections import defaultdict

from client.core.grammar import possessive
from client.core.items.container import Container
from client.core.items.item import consumable
from client.core.models.model import get_new_default_model

left = u'left '
right = u'right '

_weights = {'arm': 7, 'back': 10, 'chest': 5, 'belly': 7, 'foot': 3, 'hand': 2, 'head': 6, 'leg': 10, 'neck': 2}
_requirements = defaultdict(lambda: lambda item: True, {'back': lambda item: isinstance(item, Container)})

class Body_Part(Container):

    def __init__(self, type_, side='', ** kwargs):

        super(Body_Part, self).__init__(name=u'%s%s' % (side, type_), capacity=50, locked=True, maximum=1, model=get_new_default_model(icon=type_), nutrition_value=consumable, requirement=_requirements[type_], weight=_weights[type_], ** kwargs)

        self.__side = side
        self.__type = type_

    def __unicode__(self):

        return u'%s %s' % (possessive(self.owner), super(Body_Part, self).__unicode__())
