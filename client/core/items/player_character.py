# -*- coding: utf-8 -*-

'''
This defines the L{Player_Character} class.
'''

import __builtin__

from common.halt_on_exception import halt_on_exception

from client.core.events import proceeding, journey_state_change, player_character_action_change, player_character_target_change, player_character_perception
from client.core.grammar import capitalize, subject
from client.core.items.character import Character

class Player_Character(Character):

    def __new__(cls, ** kwargs):

        result = super(Player_Character, cls).__new__(cls, ** kwargs)

        # Set it before Character.__init__ to prevent ending up with wrong body part names.
        __builtin__.player_character = result

        return result

    def __init__(self, ** kwargs):

        assert player_character == self

        if 0 < len(kwargs):

            super(Player_Character, self).__init__(** kwargs)

            self.knowledge = set()

            self.manual_movement = None
            self.manual_rotation = None
            self.__planned_action = None

            self.__target = None

            self.accept(proceeding, self.__handle_proceeding)

    def __del__(self):

        print 'del'
        print unicode(self)

    @property
    def condition(self):

        return super(Player_Character, self).condition

    @condition.setter
    @halt_on_exception
    def condition(self, new_condition):

        Character.condition.fset(self, new_condition)

        if not self.alive:

            messenger.send(journey_state_change, [journey_state_change.player_character_death])

    @property
    @halt_on_exception
    def planned_action(self):

        if self.__planned_action != None:
            if 1 <= self.__planned_action.completion:
                self.__planned_action = None
                messenger.send(player_character_action_change)

        return self.__planned_action

    @planned_action.setter
    @halt_on_exception
    def planned_action(self, new_planned_action):

        if new_planned_action != self.planned_action:

            self.__planned_action = new_planned_action

            messenger.send(player_character_action_change)

    @property
    def target(self):

        return self.__target

    @target.setter
    @halt_on_exception
    def target(self, new_target):

        if new_target != self.__target:
            self.__target = new_target

            messenger.send(player_character_target_change)

    def __handle_proceeding(self, action):

        self.handle_bye(action)

        if action.interesting:

            messenger.send(player_character_perception, [action])

            for flag in action.flags:
                if not flag in self.knowledge:
                    self.knowledge.add(flag)
                    messenger.send(player_character_perception, [u'[%s learned that %s]' % (capitalize(subject(self)), flag)])

    def update(self, elapsed_time):

        if (self.manual_movement == None) and (self.manual_rotation == None):
            super(Player_Character, self).update(elapsed_time)

            return

        if self.manual_rotation != None:
            self.manual_rotation.update(elapsed_time)

        if self.manual_movement != None:
            self.manual_movement.update(elapsed_time)
