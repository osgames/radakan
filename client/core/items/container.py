# -*- coding: utf-8 -*-

'''
This defines the L{Container} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.grammar import capitalize, object_, subject
from client.core.items.item import Item

class Container(Item):
    '''
    This is an L{Item} that can contain other L{items<Item>}.
    '''

    def __init__(self, capacity, maximum=None, requirement=lambda item: True, weight=float('inf'), ** kwargs):

        # We'll set this first, because otherwise we could run into problems where 'weight' is used.
        self.__contents = set()

        super(Container, self).__init__(weight=weight, ** kwargs)

        self.__capacity = capacity
        self.__maximum = maximum
        self.__requirement = requirement

    def __contains__(self, item):

        return item in self.__contents

    def __iter__(self):

        for content in self.__contents:
            yield content

    def __len__(self):

        return len(self.__contents)

    @property
    def capacity(self):

        return self.__capacity

    @property
    @halt_on_exception
    def free_capacity(self):
        '''
        Return the free capacity of the container
        '''

        return self.__capacity - sum(item.size for item in self)

    @property
    @halt_on_exception
    def tooltip_text(self):

        tooltip_text = super(Container, self).tooltip_text

        if len(self) == 0:
            tooltip_text += u'\nContents: none'
        elif len(self) == 1:
            tooltip_text += u'\nContents: %s' % tuple(self)[0]
        else:
            tooltip_text += u'\nContents: %s items' % len(self)

        return tooltip_text

    @property
    @halt_on_exception
    def weight(self):

        return super(Container, self).weight + sum(item.weight for item in self)

    @property
    def maximum(self):

        return self.__maximum

    @property
    @halt_on_exception
    def nutrition_value(self):
        '''The weighted average of the component nutrition_values.'''

        try:
            return (super(Container, self).nutrition_value * super(Container, self).weight + sum(item.nutrition_value * item.weight for item in self)) / self.weight
        except ZeroDivisionError:
            return super(Container, self).nutrition_value

    @property
    def requirement(self):

        return self.__requirement

    @property
    @halt_on_exception
    def __identifier(self):

        return '{name}_{id}'.format(name=unicode(self).lower().replace(' ', '_').replace('\'', ''), id=str(id(self)).replace('L', ''))

    def add(self, item):

        self.__contents.add(item)

    def get_recontaining_problems(self, destination):

        for problem in super(Container, self).get_recontaining_problems(destination):
            yield problem

        if self in destination.container_ancestors:
            yield u'{0} can\'t be added to {1}, because it would cause an impossible loop.'.format(
                capitalize(subject(self)),
                object_(destination)
            )

    def get_save_data(self, container, ** kwargs):

        result = u'{identifier} = {item_data}'.format(
            identifier=self.__identifier,
            item_data=super(Container, self).get_save_data(
                container=container,
                capacity=self.__capacity,
                ** kwargs
            )
        )

        for item in self:

            result = u'{result}{item_save_data}'.format(
                item_save_data=item.get_save_data(self),
                container_identifier=self.__identifier
            )

        return result

    def remove(self, item):

        self.__contents.remove(item)

    def find_all(self, test=lambda item: True, recursive=False):

        for content in self:
            if test(content):
                yield content

            if recursive and isinstance(content, Container):
                for item in content.find_all(test=test, recursive=True):
                    yield item

    def find_one(self, test=lambda item: True, recursive=False):

        for element in self:
            if test(element):
                return element
            elif recursive and isinstance(element, Container):
                found = element.find_one(test=test, recursive=True)
                if found != None:
                    return found

        return None
