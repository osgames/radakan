# -*- coding: utf-8 -*-

'''
This defines the L{Container_Exception} class.
'''

class Container_Exception(Exception):
    '''
    This is an C{Exception} that occurs when an L{Item} is added to or removed from a L{Container} that doesn't allow it.
    '''

    pass
