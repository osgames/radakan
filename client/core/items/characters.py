# -*- coding: utf-8 -*-

'''
This defines the L{register_character} and L{update_living_characters} functions.
'''

from client.core.events import journey_state_change, world_update_exception
from client.core.items.container_exception import Container_Exception
from client.core.observer import Observer

__characters = set()

def register_character(character):

    __characters.add(character)

def update_living_characters(elapsed_time):
    '''Update each living L{Character}.'''

    dead_characters = set()

    global __characters
    for character in __characters:

        if character.alive:
            try:
                character.update(elapsed_time)
            except Container_Exception, exception:
                messenger.send(world_update_exception, [exception])
        else:
            dead_characters.add(character)

    __characters -= dead_characters

def __handle_journey_state_change(journey_state):

    if journey_state == journey_state_change.end:

        for character in __characters:
            character.destroy()

        global __characters
        __characters = set()

__characters_observer = Observer()
__characters_observer.accept(journey_state_change, __handle_journey_state_change)
