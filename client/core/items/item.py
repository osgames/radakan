# -*- coding: utf-8 -*-

'''
This defines the L{Item} class.
'''

from common.halt_on_exception import halt_on_exception
from common.percentage import percentage

from client.core.events import condition_change, recontainment
from client.core.grammar import capitalize, object_, subject
from client.core.items.container_exception import Container_Exception
from client.core.models.model import get_new_default_model

# these are epressed in 'full_health / m^3'
unconsumable = - 10000 # death after eating 100 cm^3
consumable = 1000 # full health after eating 1000 cm^3

class Item(object):
    '''
    This is a physical object in the game world.
    '''

    def __init__(self, name, container, weight=float('inf'), description=None, locked=False, model=None, damage_modifier=1, nutrition_value=unconsumable, owner=None, size=None):

        self.__name = unicode(name)

        super(Item, self).__init__()

        self.__condition = 1.0

        self.__damage_modifier = damage_modifier
        self.__description = description
        self.locked = False # Make sure the container can be set.
        self.__nutrition_value = float(nutrition_value)
        self.__weight = float(weight)

        self.gui_widgets = list()
        self.__owner = owner

        if size == None:
            # organic material: about 1 fist(kg) per jar(liter)
            self.__size = self.__weight
        else:
            self.__size = float(size)

        if model == None:
            model = get_new_default_model()

        self.__container = container

        model.item = self
        self.model = model

        if container != None:
            container.add(self)

        self.locked = locked

    def __unicode__(self):

        return self.__name

    @property
    def alive(self):

        return False

    @property
    def attack_modifier(self):

        return 1

    @property
    def container(self):

        return self.__container

    @container.setter
    @halt_on_exception
    def container(self, destination):
        '''Set my container to the given destination.'''

        problems = tuple(self.get_recontaining_problems(destination))

        if 0 < len(problems):
            raise Container_Exception, '\n'.join(problems)

        source = self.container

        self.container.remove(self)
        destination.add(self)

        self.__container = destination

        messenger.send(recontainment, [self, source])

    @property
    @halt_on_exception
    def container_ancestors(self):

        ancestor = self.container
        while ancestor != None:
            yield ancestor
            ancestor = ancestor.container

    @property
    def coordinates(self):

        return self.model_item.model.coordinates

    @coordinates.setter
    @halt_on_exception
    def coordinates(self, new_coordinates):

        assert self.model == self.model_item.model, u'The coordinates of a contained item (\'{self}\') can\'t be set.'.format(** vars())

        self.model.coordinates = new_coordinates

    @property
    def condition(self):

        return self.__condition

    @condition.setter
    @halt_on_exception
    def condition(self, new_condition):

        self.__condition = min(max(0, new_condition), 1)

        messenger.send(condition_change, [self])

    @property
    def damage_modifier(self):

        return self.__damage_modifier

    @property
    def defence(self):
        '''This is the measure of how well the item can defend itself.'''

        return 0

    @property
    def defence_modifier(self):
        '''This is the measure of how well a character can defend itself using this item.'''

        return 1

    @property
    def description(self):

        return self.__description

    @property
    def dynamic_player_character_actions(self):

        return tuple()

    @property
    @halt_on_exception
    def model_item(self):

        if self.container.model_item == None:
            return self
        else:
            return self.container.model_item

    @property
    def nutrition_value(self):

        return self.__nutrition_value

    @property
    @halt_on_exception
    def owner(self):

        if self.__owner == None:
            return self.container.owner

        else:
            return self.__owner

    @property
    def size(self):

        return self.__size

    @property
    @halt_on_exception
    def tooltip_text(self):

        tooltip_text = u''

        if self.description != None:
            tooltip_text = u'%s\n\n' % self.description

        tooltip_text += u'Condition: %s' % percentage(self.condition)
        if hasattr(self, 'race'):
            if self.alive:
                tooltip_text += u' - alive'
            else:
                tooltip_text += u' - dead'

        tooltip_text += u'\nSize: %.1f jars' % self.size

        if self.weight < float('inf'):
            tooltip_text += u'\nWeight: %.1f fist' % self.weight

        if not self.owner in (None, player_character):
            tooltip_text += u'\nOwner: %s' % self.owner

        return tooltip_text

    @property
    def weight(self):

        return self.__weight

    def get_distance(self, other):

        return self.model.get_distance(other)

    def get_recontaining_problems(self, container):
        '''This does not include completion, to match the action problems.'''

        if (container.maximum != None) and (container.maximum <= len(container)):
            yield u'{0} can\'t be added to {1}, because {1} already contains {2} item{3}.'.format(
                capitalize(subject(self)),
                object_(container),
                container.maximum,
                ('' if len(container) == 1 else 's')
            )

        if self.locked:
            yield u'{0} can\'t be added to {1}, because {2} is locked in {3}.'.format(
                capitalize(subject(self)),
                object_(container),
                subject(self),
                object_(self.container)
            )

        if not container.requirement(self):
            yield u'{0} can\'t be added to {1}, because {2} doesn\'t match the requirement of {1}.'.format(
                capitalize(subject(self)),
                object_(container),
                subject(self)
            )

        if container.free_capacity < self.size:
            yield u'{0} can\'t be added to {1}, because {2} is to large to fit into the available space in {1} ({3} jars required and only {4} jars available).'.format(
                capitalize(subject(self)),
                object_(container),
                subject(self),
                self.size,
                container.free_capacity
            )

    def get_save_data(self, container_identifier, ** kwargs):

        return u'{type_name}(model={model}, nutrition_value={self.nutrition_value}, description={self.description!r}, name={name!r}, weight={weight}, owner={self.owner}, container={container_identifier}, locked={self.locked}, {kwargs_string})\n'.format(
            container_identifier=container_identifier,
            kwargs_string=u''.join(', {key}={value}'.format(key=key, value=value) for key, value in kwargs.iteritems()),
            weight=self.__weight,
            model=self.model.get_save_data(),
            name=unicode(self),
            self=self,
            type_name=type(self).__name__
        )
