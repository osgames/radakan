# -*- coding: utf-8 -*-

'''
This defines the L{Non_Player_Character} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.actions.damage import Damage
from client.core.actions.destroy import Destroy
from client.core.actions.offer import Offer
from client.core.actions.rotate import Rotate
from client.core.actions.speak import Speak
from client.core.actions.take import Take
from client.core.events import proceeding
from client.core.tracker import quests_completed
from client.core.items.character import Character

bye = 'bye'
hello = 'hello'
how_are_you = 'how are you'
where_am_i = 'where am i'

# 0: short term survival
# 1: reactions to non-critical events
# 2: long term survival
_priorities = range(0, 3)

class Non_Player_Character(Character):

    def __init__(self, ** kwargs):

        super(Non_Player_Character, self).__init__(** kwargs)

        # Reactions are sorted by priority and from old to new.
        self.__planned_reactions = dict(((priority, list()) for priority in _priorities))

        self.target = None

        self.accept(proceeding, self.__handle_proceeding)

    @property
    @halt_on_exception
    def planned_action(self):

        # Prioritize important reactions.
        for priority in _priorities:

            planned_reactions_layer = self.__planned_reactions[priority]

            while 0 < len(planned_reactions_layer):

                if 1 <= planned_reactions_layer[- 1].completion:
                    # Remove a reaction if it already has been completed.
                    planned_reactions_layer.pop()
                else:
                    # Return the reaction to most recent event.
                    return planned_reactions_layer[- 1]

        if self.target != None:

            if 30 < self.get_distance(self.target):

                self.target = None

            else:

                rotate = Rotate(self, self, target=self.target)
                if rotate.completion < 1:
                    return rotate

        # explicit, because it may occur
        return None

    @property
    def reactions(self):

        return self.__reactions

    def __handle_proceeding(self, action):

        self.handle_bye(action)

        if action.interesting and (action.actor != self) and (self in action.involved_items):

            self.process_action(action)

    def plan_reaction(self, level, action):

        self.__planned_reactions[level].append(action)

    def process_action(self, action):

        if isinstance(action, Damage):

            if (self in (action.item, action.item.owner)):

                self.target = action.actor

                self.plan_reaction(0, Destroy(
                    self,
                    item=action.actor
                ))

        elif isinstance(action, Offer):

            if action.target == self:

                self.target = action.item

                self.plan_reaction(1, Take(
                    actor=self,
                    item=action.item
                ))

                self.plan_reaction(1, Speak(
                    actor=self,
                    content=u'Thanks!',
                    target=action.actor
                ))

                # TODO Find a better place.
                quests_completed.value += 1

        elif isinstance(action, Speak):

            if action.target == self:

                if action.topic == hello:

                    self.target = action.actor

                    self.plan_reaction(1, Speak(
                        self,
                        content=u'Hello.',
                        target=action.actor
                    ))

                elif action.topic == bye:

                    self.plan_reaction(1, Speak(
                        self,
                        content=u'Bye.',
                        target=action.actor,
                        topic=bye
                    ))

                elif action.topic == how_are_you:

                    self.plan_reaction(1, Speak(
                        self,
                        content=u'I\'m fine.',
                        target=action.actor
                    ))

                elif action.topic != None:

                    self.plan_reaction(1, Speak(
                        self,
                        content=u'I don\'t understand what you just tried to say.',
                        target=action.actor
                    ))

    @property
    @halt_on_exception
    def dynamic_player_character_actions(self):

        # Is the player character to dumb to speak properly?
        if float(player_character.charisma) < 0.6:

            # Return a tupple to prevent options being added.
            return (Speak(
                player_character,
                content=u'Ugh...',
                impressions=(u'dumb',),
                target=self
            ),)

        # Am I listening to the player character?
        elif self.target == player_character:

            possible_actions = set()

            possible_actions.add(Speak(
                player_character,
                content=u'How are you doing?',
                target=self,
                tone=u'ask',
                topic=how_are_you
            ))
            possible_actions.add(Speak(
                player_character,
                content=u'Where am I?',
                target=self,
                tone=u'ask',
                topic=where_am_i
            ))

            possible_actions.add(Speak(
                player_character,
                content=u'Bye!',
                target=self,
                topic=bye
            ))

            return possible_actions

        else:

            # Return a tupple to prevent options being added.
            return (Speak(
                player_character,
                content=u'Hi!',
                target=self,
                topic=hello
            ),)
