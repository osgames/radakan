# -*- coding: utf-8 -*-

'''
This defines the L{Character} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.actions.speak import Speak
from client.core.events import proceeding
from client.core.items.body_part import Body_Part, left, right
from client.core.items.characters import register_character
from client.core.items.composite_item import Composite_Item
from client.core.items.container_exception import Container_Exception
from client.core.items.item import Item
from client.core.observer import Observer
from client.core.tracker import deaths_caused
from client.core.skills.skillset import number_of_skills, Skillset

def _get_skill(skill_name):

    skill_name = skill_name.lower()

    return getattr(__import__('client.core.skills.skills', fromlist=skill_name), skill_name)

class Character(Composite_Item, Observer):
    '''
    This is an L{Composite_Item} with a behavior.
    '''

    def __init__(self, gender, race, skill_levels=dict(), ** kwargs):

        super(Character, self).__init__(owner=None, ** kwargs)

        self.head = Body_Part(type_=u'head', container=self)
        self.neck = Body_Part(type_=u'neck', container=self)
        self.chest = Body_Part(type_=u'chest', container=self)
        self.belly = Body_Part(type_=u'belly', container=self)
        self.back = Body_Part(type_=u'back', container=self)
        self.left_arm = Body_Part(side=left, type_=u'arm', container=self)
        self.right_arm = Body_Part(side=right, type_=u'arm', container=self)
        self.left_hand = Body_Part(side=left, type_=u'hand', container=self)
        self.right_hand = Body_Part(side=right, type_=u'hand', container=self)
        self.left_leg = Body_Part(side=left, type_=u'leg', container=self)
        self.right_leg = Body_Part(side=right, type_=u'leg', container=self)
        self.left_foot = Body_Part(side=left, type_=u'foot', container=self)
        self.right_foot = Body_Part(side=right, type_=u'foot', container=self)

        # Looking for 'health'? See 'condition' in 'item.py'.
        self.__gender = gender
        self.__race = race

        if len(skill_levels) < number_of_skills:
            skill_levels = Skillset(race, ** skill_levels).skill_levels

        for skill, skill_level in skill_levels.iteritems():

            self.__dict__[str(unicode(skill)).lower()] = skill_level

        register_character(self)

    @property
    def alive(self):

        return 0 < self.condition

    @property
    def attack(self):

        return self.weapon.attack_modifier * 0.5

    @property
    def backpack(self):

        if len(self.back) == 0:
            # Since we don't have a backpack, use a hand instead.
            return self.right_hand
        else:
            return tuple(self.back)[0]

    @property
    def condition(self):

        return super(Character, self).condition

    @condition.setter
    @halt_on_exception
    def condition(self, new_condition):

        # Strong muscles prevent damage.
        new_condition = self.condition + (new_condition - self.condition) / float(self.strength)

        previously_alive = self.alive

        Composite_Item.condition.fset(self, new_condition)

        if previously_alive and not self.alive:

            self.ignoreAll()

            self.model.fall()

            deaths_caused.value += 1

    @property
    def defence(self):

        return self.weapon.defence_modifier * 0.5

    @property
    def gender(self):

        return self.__gender

    @property
    def owner(self):

        return self

    @property
    def race(self):

        return self.__race

    @property
    def storage(self):

        if 1 <= len(self.back):

            return self.back.find_one(recursive=False)

        else:

            return self.right_hand

    @property
    def weapon(self):

        if len(self.right_hand) == 0:
            # Since we don't have a weapon, use a hand instead.
            return self.right_hand
        else:
            return tuple(self.right_hand)[0]

    def can_perceive(self, action_or_event_or_item):

        if isinstance(action_or_event_or_item, Item):

            item = action_or_event_or_item

            try:
                # A character can perceive closeby items or large items in the distance.
                return self.can_reach(item) or self.get_distance(item) <= 500 * item.size
            except AttributeError:
                # 'item' is the world.
                pass

            return False

        else: # if isinstance(item_or_event_or_action, (Action, Event)):

            action_or_event = action_or_event_or_item

            return any(self.can_perceive(item) for item in action_or_event.involved_items)

    def can_reach(self, item):
        ''''item' may be a L{Vector3} or an L{Item}.'''

        radius = 0
        if isinstance(item, Item):
            radius = item.model_item.model.radius

        return self.get_distance(item) <= 1.5 + radius

    def destroy(self):

        self.ignoreAll()

    def update(self, elapsed_time):
        '''
        Let 'self' (re)act.
        '''

        if self.planned_action != None:

            # This problem frequently returns.
            assert self.planned_action.completion < 1, u'\'%s\' is already completed (%s).' % (self.planned_action, self.planned_action.completion)

            to_do = self.planned_action.to_do
            try:
                to_do.update(elapsed_time)
            except Container_Exception:
                raise
            else:
                # Use the old 'to_do'. 'self.planned_action.to_do' may have changed.
                messenger.send(proceeding, [to_do])

    def handle_bye(self, action):

        from client.core.items.non_player_character import bye

        if isinstance(action, Speak) and (action.actor == self) and (action.topic == bye):

            # Stop focusing on 'target' when saying goodbye.
            self.target = None
