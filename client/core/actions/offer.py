# -*- coding: utf-8 -*-

'''
This defines the L{Offer} class.
'''

from client.core.actions.speak import Speak
from client.core.actions.take import Take

class Offer(Speak):
    '''
    This is an L{Action} for a L{Character} that offers an L{Item} to its target.
    '''

    def __init__(self, actor, item, ** kwargs):

        super(Offer, self).__init__(actor, ** kwargs)

        self.item = item

        self.__requirements = [Take(actor, item=item)]
        self.__requirements.extend(super(Offer, self).requirements)

    def __unicode__(self):

        return u'%s offers %s to %s: %s' % (self.actor, self.item, self.target, super(Offer, self).__unicode__())

    @property
    def involved_items(self):

        return set([self.item, self.item.owner]) | super(Offer, self).involved_items - set([None])

    requirements = property(lambda self_: self_.__requirements)
