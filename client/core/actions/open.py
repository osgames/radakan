# -*- coding: utf-8 -*-

'''
This defines the L{Open} class.
'''

from client.core.actions.action import Action
from client.core.actions.approach import Approach
from client.core.items.container import Container

class Open(Action):
    '''
    This is an L{action<Action>} for a L{character<Character>} that opens an L{container<Container>}.
    '''

    def __init__(self, actor, item=None):

        super(Open, self).__init__(actor)

        if item == None:
            item = actor.target

        self.item = item

        self.__requirements = (Approach(actor, destination=item), )

    @property
    def interesting(self):

        return False

    @property
    def involved_items(self):

        return set([self.actor, self.item])

    @property
    def requirements(self):

        return self.__requirements

    @staticmethod
    def does_apply(actor):

        return isinstance(actor.target, Container)
