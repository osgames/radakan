# -*- coding: utf-8 -*-

'''This defines the L{Damage} class.'''

from random import random

from client.core.actions.approach import Approach
from client.core.actions.action import Action

class Damage(Action):
    '''
    This is an L{Action} for a L{Character} that damages an L{Item}.
    '''

    def __init__(self, actor, item=None):

        super(Damage, self).__init__(actor)

        if item == None:
            item = actor.target

        self.item = item

        self.__requirements = (Approach(actor, destination=item),)

    @property
    def involved_items(self):

        return set([self.actor, self.item, self.item.owner]) - set([None])

    @property
    def requirements(self):

        return self.__requirements

    @staticmethod
    def does_apply(not_used):

        return True

    def update(self, elapsed_time):

        if random() < self.actor.attack * (1 - self.item.defence):
            self.item.condition -= self.actor.strength * self.actor.weapon.damage_modifier * random() * 10 * elapsed_time / self.item.weight

            # Mark the action as completed.
            super(Damage, self).update(elapsed_time)
