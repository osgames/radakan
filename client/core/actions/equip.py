# -*- coding: utf-8 -*-

'''
This defines the L{Equip} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.actions.approach import Approach
from client.core.actions.recontain import Recontain

class Equip(Recontain):
    '''
    This is an L{Action} for a L{Character} that equips an L{Item}.
    '''

    def __init__(self, actor, item=None):

        super(Equip, self).__init__(actor, item=item, destination=actor.right_hand)

        self.__requirements = [Approach(actor, destination=self.item)]
        if actor.right_hand.maximum <= len(actor.right_hand):
            self.__requirements.append(Recontain(actor, item=list(actor.right_hand)[0], destination=actor.storage))

    @property
    def requirements(self):

        return self.__requirements

    @property
    @halt_on_exception
    def problems(self):

        # Ignore that the target may already contain the maximal number of items.
        for problem in super(Equip, self).problems:
            if not 'already contains' in problem:
                yield problem
