# -*- coding: utf-8 -*-

'''
This defines the L{Recontain} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.actions.action import Action
from client.core.grammar import capitalize, conjugate, object_, subject

class Recontain(Action):
    '''
    This is an L{Action} for a L{Character} that is recontaining an L{Item}.
    '''

    def __init__(self, actor, destination, item=None):

        super(Recontain, self).__init__(actor)

        if item == None:
            item = actor.target

        self.item = item
        self.source = item.container
        self.destination = destination

    def __unicode__(self):

        return u'%s %s %s.' % (capitalize(subject(self.actor)), conjugate(type(self).__name__.lower(), self.actor), object_(self.item))

    @property
    def completion(self):

        if self.item in self.destination:
            return 1
        else:
            return 0

    @property
    def involved_items(self):

        return set([self.actor, self.item, self.source, self.destination])

    @property
    def requirements(self):

        return tuple()

    @staticmethod
    def does_apply(actor):

        return not actor.target.alive

    @property
    @halt_on_exception
    def problems(self):

        return self.item.get_recontaining_problems(self.destination)

    def update(self, elapsed_time):

        self.item.container = self.destination
