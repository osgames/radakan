# -*- coding: utf-8 -*-

'''
This defines the L{Take_All} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.actions.action import Action
from client.core.actions.approach import Approach
from client.core.actions.take import Take
from client.core.grammar import object_

class Take_All(Action):

    def __init__(self, actor, container=None):

        super(Take_All, self).__init__(actor)

        self.__container = container

        self.__maximal_number_of_items = len(tuple(self.__items))

        self.__approach = Approach(actor, destination=container)

    @property
    @halt_on_exception
    def completion(self):

        self.__maximal_number_of_items = max(self.__maximal_number_of_items, len(tuple(self.__items)))
        return max(1 - float(len(tuple(self.__items))) / self.__maximal_number_of_items, 0)

    @property
    @halt_on_exception
    def long_command(self):

        return u'Take all items in %s.' % object_(self.__container)

    @property
    @halt_on_exception
    def __items(self):

        return self.__container.find_all(test=(lambda item: len(tuple(item.get_recontaining_problems(self.actor.backpack))) == 0), recursive=True)

    @property
    def involved_items(self):

        return set(self.__items) + set([self.actor])

    @property
    @halt_on_exception
    def problems(self):

        if len(tuple(self.__items)) == 0:
            yield u'There are no items to take in %s.' %  self.__container

    @property
    @halt_on_exception
    def requirements(self):

        yield self.__approach

        for item in self.__items:
            yield Take(self.actor, item)

    def update(self, elapsed_time):

        pass
