# -*- coding: utf-8 -*-

'''
This defines the L{Destroy} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.actions.damage import Damage
from client.core.items.character import Character

class Destroy(Damage):
    '''
    This is an L{Action} for a L{Character} that destroys an L{Item}.
    '''

    def __init__(self, actor, ** kwargs):

        super(Destroy, self).__init__(actor, ** kwargs)

    @property
    @halt_on_exception
    def completion(self):

        try:
            return 1 - self.item.condition
        except AttributeError, error:
            self.item = None

            return 1

    @property
    def verb(self):

        if isinstance(self.item, Character):
            return u'kill'
        else:
            return super(Destroy, self).verb

    @staticmethod
    def does_apply(actor):

        return True
