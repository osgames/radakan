# -*- coding: utf-8 -*-

'''
This defines the L{Drop} class.
'''

from client.core.actions.recontain import Recontain

class Drop(Recontain):
    '''
    This is an L{Action} for a L{Character} that drops an L{Item}.
    '''

    def __init__(self, actor, item=None):

        super(Drop, self).__init__(actor, item=item, destination=actor.container)

    @property
    def requirements(self):

        return tuple()

    @staticmethod
    def does_apply(actor):

        return actor.target in actor.find_all(recursive=True)
