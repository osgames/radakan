# -*- coding: utf-8 -*-

'''
This defines the L{Walk} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.actions.displace import Displace
from client.core.grammar import object_

class Walk(Displace):
    '''
    This is an L{Action} for a L{Character} that walks.
    '''

    def __init__(self, actor, direction):

        self.__direction = direction

        super(Walk, self).__init__(actor, item=actor, destination=actor)

    @property
    @halt_on_exception
    def completion(self):

        return 0

    @property
    @halt_on_exception
    def sentence_end(self):

        self.__update_destination()

        return u'to %s.' % object_(self.destination)

    @property
    def requirements(self):

        return tuple()

    def update(self, elapsed_time):

        self.__update_destination()

        super(Walk, self).update(elapsed_time)

    def __update_destination(self):

        self.destination = self.actor.coordinates + self.actor.model.forward * 1000 * self.__direction
