# -*- coding: utf-8 -*-

'''
This defines the L{Action} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.grammar import capitalize, conjugate, object_, subject

class Action(object):
    '''
    This is a base class for classes that may serve as a U{strategy<http://en.wikipedia.org/wiki/Strategy_pattern>} for a L{Character}.
    '''

    def __init__(self, actor, flags=tuple()):

        super(Action, self).__init__()

        self.actor = actor

        self.flags = flags

        self.__completion = 0

    def __unicode__(self):

        return self.long_command.replace(capitalize(self.verb), u'%s %s' % (capitalize(subject(self.actor)), conjugate(self.verb, self.actor)), 1)

    @property
    def long_command(self):

        return (u'%s %s %s.' % (self.tags, capitalize(self.verb), self.sentence_end)).lstrip()

    @property
    @halt_on_exception
    def object_(self):

        return object_(self.item)

    @property
    @halt_on_exception
    def sentence_end(self):

        return object_(self.item)

    @property
    @halt_on_exception
    def short_command(self):

        return capitalize(self.verb)

    @property
    @halt_on_exception
    def tags(self):

        return u''

    @property
    def verb(self):

        return unicode(type(self).__name__.replace('_', ' ').lower())

    @property
    def completion(self):

        return self.__completion

    @property
    def interesting(self):

        return True

    @property
    def involved_items(self):

        return set([self.actor, self.target])

    @property
    @halt_on_exception
    def problems(self):
        '''This does not include completion.'''

        for requirement in self.requirements:
            for problem in requirement.problems:
                yield problem

    @property
    @halt_on_exception
    def to_do(self):
        '''This is an either an uncompleted recursive C{requirement}, or C{self} (when all C{requirements} are completed).'''

        assert self.completion < 1, u'\'%s\' is already completed.' % self

        for requirement in self.requirements:
            if requirement.completion < 1:
                return requirement.to_do

        return self

    def update(self, elapsed_time):

        self.__completion = 1
