# -*- coding: utf-8 -*-

'''
This defines the L{Consume} class.
'''

from client.core.actions.action import Action
from client.core.actions.destroy import Destroy
from client.core.actions.take import Take

class Consume(Action):
    '''
    This is an L{Action} for a L{Character} that consumes an L{Item}.
    '''

    def __init__(self, actor, item=None):

        super(Consume, self).__init__(actor)

        if item == None:
            item = actor.target

        self.__requirements = (Take(actor, item=item), Destroy(actor, item=item))

        self.item = item

        # The item may be demolished, so we have to remember this.
        self.__nutrition_value = item.nutrition_value

        self.__size = item.size
        self.__original_size = self.__size

    @property
    def completion(self):

        return 1 - float(self.__size) / self.__original_size

    @property
    def winvolved_items(self):

        return set([self.actor, self.item])

    @property
    def requirements(self):

        return self.__requirements

    @staticmethod
    def does_apply(actor):

        return not actor.target.alive

    def update(self, elapsed_time):

        amount = min(elapsed_time, self.__size)

        self.actor.hunger -= self.item.nutrition_value * amount

        self.__size -= amount
