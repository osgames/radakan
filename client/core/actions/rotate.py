# -*- coding: utf-8 -*-

'''
This defines the L{Rotate} class.
'''

from math import cos

from common.halt_on_exception import halt_on_exception

from client.core.actions.action import Action
from client.core.grammar import capitalize, conjugate, object_, subject

def _normalized(angle):

    angle = angle % 360

    if 180 < angle:

        angle = angle - 360

    assert abs(angle) <= 180, u'Angle %s is more than a half rotation.' % angle

    return angle

class Rotate(Action):
    '''
    This is an L{Action} for a L{Character} that rotates an L{Item}.
    '''

    def __init__(self, actor, item, angle=None, target=None):

        super(Rotate, self).__init__(actor)

        assert (angle == None) != (target == None), u'Either \'angle\' (%s) or \'target\' (%s) should be \'None\'.' % (angle, target)

        self.item = item

        if target == None:
            self.__angle = _normalized(angle)
        else:
            self.__target = target

    def __unicode__(self):

        return u'%s %s %s by %s degrees.' % (capitalize(subject(self.actor)), conjugate(type(self).__name__.lower(), self.actor), object_(self.item), self.angle)

    @property
    @halt_on_exception
    def angle(self):

        if hasattr(self, '_Rotate__angle'):
            return self.__angle
        else:
            return _normalized(self.item.model.get_horizontal_angle(self.__target.coordinates))

    @property
    def completion(self):

        if abs(self.angle) < 0.01:
            return 1
        else:
            return cos(self.angle)

    @property
    def interesting(self):

        return False

    @property
    def involved_items(self):

        return set([self.actor, self.item])

    @property
    def requirements(self):

        return tuple()

    def update(self, elapsed_time):

        maximal_rotation = self.actor.strength * 250 * elapsed_time

        angle = min(self.angle, maximal_rotation)
        angle = max(angle, - maximal_rotation)

        self.item.model.horizontal_angle -= angle
