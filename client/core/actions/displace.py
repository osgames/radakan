# -*- coding: utf-8 -*-

'''
This defines the L{Displace} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.actions.action import Action
from client.core.grammar import capitalize, conjugate, object_, subject

class Displace(Action):
    '''
    This is an L{Action} for a L{Character} that displaces an L{Item}.
    '''

    def __init__(self, actor, item, destination):

        # 'super()' may not be an Action object.
        Action.__init__(self, actor)

        self.item = item
        self.source = item.container
        self.destination = destination

        self.__original_distance = item.get_distance(destination)

    def __unicode__(self):

        return u'%s %s %s to %s.' % (capitalize(subject(self.actor)), conjugate(type(self).__name__.lower(), self.actor), object_(self.item), object_(self.destination))

    @property
    @halt_on_exception
    def completion(self):

        # Even when the distance is larger than the original, 'completion' should not be negative.
        return 1 - min(self.item.get_distance(self.destination) / self.__original_distance, 1)

    @property
    def interesting(self):

        return False

    @property
    def involved_items(self):

        return set([self.actor, self.item, self.source, self.destination])

    def update(self, elapsed_time):

        displacement = self.destination.coordinates - self.item

        # 4 meter per second (= 14.4 kilometer per hour) for an actor of 70 kg
        maximal_distance = self.actor.strength * 280 * elapsed_time / self.actor.weight
        if maximal_distance < displacement.length:
            displacement = displacement.normalized * maximal_distance

        self.item.coordinates += displacement
