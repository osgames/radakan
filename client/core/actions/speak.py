# -*- coding: utf-8 -*-

'''
This defines the L{Speak} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.actions.action import Action
from client.core.actions.approach import Approach
from client.core.grammar import object_

class Speak(Action):
    '''
    This is an L{Action} for a L{Character} that speaks.
    '''

    def __init__(self, actor, content, target=None, impressions=None, topic=None, tone=u'say', flags=tuple()):

        super(Speak, self).__init__(actor, flags=flags)

        if impressions == None:
            impressions = tuple()

        self.__content = content
        self.__impressions = impressions

        if target == None:
            target = actor.target
        self.__target = target

        self.__tone = tone
        self.__topic = topic

        self.__requirements = (Approach(actor, destination=target), )

    @property
    @halt_on_exception
    def tags(self):

        if 0 < len(self.impressions):
            return u'[%s]' % (u', '.join(self.impressions))
        else:
            return u''

    @property
    @halt_on_exception
    def sentence_end(self):

        return u'"%s" to %s' % (self.__content, object_(self.target))

    @property
    @halt_on_exception
    def short_command(self):

        return u'%s "%s"' % (super(Speak, self).short_command, self.__content)

    @property
    @halt_on_exception
    def verb(self):

        return self.__tone

    @property
    def impressions(self):

        return self.__impressions

    @property
    def requirements(self):

        return self.__requirements

    @property
    def target(self):

        return self.__target

    @property
    def topic(self):

        return self.__topic
