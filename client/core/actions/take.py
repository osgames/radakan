# -*- coding: utf-8 -*-

'''
This defines the L{Take} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.actions.approach import Approach
from client.core.actions.recontain import Recontain
from client.core.grammar import conjugate, object_

class Take(Recontain):
    '''
    This is an L{Action} for a L{Character} that takes an L{Item}.
    '''

    def __init__(self, actor, item=None):

        super(Take, self).__init__(actor, item=item, destination=actor.storage)

        self.__requirements = (Approach(actor, destination=self.item),)

    def __unicode__(self):

        return super(Take, self).__unicode__().replace(".", " and %s it in %s." % (conjugate("put", self.actor), object_(self.destination)))

    @property
    def requirements(self):

        return self.__requirements

    @property
    @halt_on_exception
    def verb(self):

        if self.item.owner in (None, self.actor):
            return super(Take, self).verb
        else:
            return u'steal'

    @staticmethod
    def does_apply(actor):

        return actor.target.model_item != actor
