# -*- coding: utf-8 -*-

'''
This defines the L{Approach} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.actions.displace import Displace
from client.core.actions.rotate import Rotate
from client.core.grammar import object_

class Approach(Displace, Rotate):
    '''
    This is an L{Action} for a L{Character} that approaches its destination.
    '''

    def __init__(self, actor, destination=None):

        if destination == None:
            destination = actor.target

        Displace.__init__(self, actor, item=actor, destination=destination)
        Rotate.__init__(self, actor, item=actor, target=destination)

    @property
    @halt_on_exception
    def completion(self):

        rotation_completion = Rotate.completion.fget(self)

        if self.actor.can_reach(self.destination):
            return rotation_completion
        else:
            return min(super(Approach, self).completion, rotation_completion)

    @staticmethod
    def does_apply(not_used):

        return True

    @property
    @halt_on_exception
    def sentence_end(self):

        return object_(self.destination)

    @property
    def requirements(self):

        return tuple()

    def update(self, elapsed_time):

        if self.actor.coordinates != self.destination.coordinates:
            # Rotating first makes most sense.
            Rotate.update(self, elapsed_time)

        if not self.actor.can_reach(self.destination):
            Displace.update(self, elapsed_time)
