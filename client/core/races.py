# -*- coding: utf-8 -*-

from client.core.genders import female, genders
from client.core.race import Race

arach = Race(u'arach', u'Arachs are a centaur-like species, but with the lower body of a spider.', genders)

grogg = Race(u'grogg', u'Grogg are genetically disposed to a robust bone structure and can easily grow muscle mass at a rapid rate given enough food and active exercise.', genders)

harpy = Race(u'harpy', u'Harpies are a women-only race. Their womanly figure is supported by big talons instead of feet, with vicious looking claws.', (female,))

xemna = Race(u'xemna', u'Xemnas are a bipedal race with an imposing horn which protrudes from their foreheads.', genders)

races = (arach, xemna, grogg, harpy)
