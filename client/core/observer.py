# -*- coding: utf-8-*-

try:
    from direct.showbase.DirectObject import DirectObject
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

class Observer(DirectObject, object):

    def __init__(self):

        DirectObject.__init__(self)
        object.__init__(self)
