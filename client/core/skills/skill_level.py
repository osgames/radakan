# -*- coding: utf-8 -*-

'''
This defines the L{Skill_Level} class.
'''

from common.halt_on_exception import halt_on_exception

class Skill_Level(object):

    __experience_gain = 0.01

    def __init__(self):

        super(Skill_Level, self).__init__()

        self.__experience = 1

    @halt_on_exception
    def __float__(self):

        return self.giftedness * self.cap * self.experience / (self.cap + self.experience - 1)

    def __add__(self, other):

        return float(self) + other

    def __neg__(self):

        return - float(self)

    def __mul__(self, other):

        return float(self) * other

    def __div__(self, other):

        return float(self) / other

    def __pow__(self, other):

        return float(self) ** other

    def __cmp__(self, other):

        return cmp(float(self), other)

    @property
    def experience(self):

        return self.__experience

    def add_experience(self, amount=1):

        self.__experience += amount * Skill_Level.__experience_gain
