# -*- coding: utf-8 -*-

'''
This defines the L{Secondary_Skill_Level} class.
'''

from common.halt_on_exception import halt_on_exception

from client.core.skills.skill_level import Skill_Level

class Secondary_Skill_Level(Skill_Level):

    def __init__(self, influencing_skills=tuple()):

        super(Secondary_Skill_Level, self).__init__()

        self.__influencing_skills = influencing_skills

    @property
    def cap(self):

        return 4

    @property
    @halt_on_exception
    def giftedness(self):

        return reduce(lambda x, y: float(x) * float(y), self.__influencing_skills) ** (1.0 / len(self.__influencing_skills))
