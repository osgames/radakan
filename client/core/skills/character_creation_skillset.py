# -*- coding: utf-8 -*-

from common.percentage import percentage

from client.core.skills.skills import primary_skills
from client.core.skills.skillset import Skillset

_epsilon = 0.001

class Character_Creation_Skillset(Skillset):

    primary_skill_point_step = 0.2
    minimum_primary_skill_points = 2

    def __init__(self, race, ** kwargs):

        super(Character_Creation_Skillset, self).__init__(race, ** kwargs)

    def can_raise_skill(self, skill):

        return 0 < self.get_skill_points_left()

    def can_lower_skill(self, skill):

        return type(self).minimum_primary_skill_points * type(self).primary_skill_point_step + _epsilon < self.skill_levels[skill].points

    def get_skill_percentage(self, skill):

        return percentage(float(self.skill_levels[skill]))

    def get_skill_points_left(self):

        return int(round((len(primary_skills) - sum(self.skill_levels[skill].points for skill in primary_skills)) / type(self).primary_skill_point_step))

    def get_skill_points(self, skill):

        return int(round(self.skill_levels[skill].points / type(self).primary_skill_point_step))

    def lower_skill(self, skill):

        assert self.can_lower_skill(skill), u'Skill {skill} can\'t be lowered.'.format(** vars())

        self.skill_levels[skill].points -= type(self).primary_skill_point_step

    def raise_skill(self, skill):

        assert self.can_raise_skill(skill), u'Skill {skill} can\'t be raised.'.format(** vars())

        self.skill_levels[skill].points += type(self).primary_skill_point_step

    def reset(self, minima=False):

        points = float(1)

        if minima:
            points = type(self).minimum_primary_skill_points * type(self).primary_skill_point_step

        for skill in primary_skills:

            self.skill_levels[skill].points = points
