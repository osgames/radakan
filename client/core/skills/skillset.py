# -*- coding: utf-8 -*-

from client.core.skills.primary_skill_level import Primary_Skill_Level
from client.core.skills.secondary_skill_level import Secondary_Skill_Level
from client.core.skills.skills import primary_skills, social_skills, fighting_skills, thieving_skills

number_of_skills = sum(len(skill_group) for skill_group in (primary_skills, social_skills, fighting_skills, thieving_skills))

class Skillset(object):

    def __init__(self, race, ** kwargs):

        super(Skillset, self).__init__()

        self.skill_levels = dict()

        for skill in primary_skills:

            self.skill_levels[skill] = Primary_Skill_Level(skill.race_modifiers[race])

        for skill_group in (social_skills, fighting_skills, thieving_skills):

            for skill in skill_group:

                self.skill_levels[skill] = Secondary_Skill_Level(
                    tuple(self.skill_levels[influencing_skill] for influencing_skill in skill.influencing_skills)
                )

        for skill_name, skill_giftedness in kwargs.iteritems():

            self.skill_levels[skill_name].giftedness = float(skill.modifiers.get(self.race, 1)) * skill_giftedness

    def set_race(self, race):

        for skill in primary_skills:

            self.skill_levels[skill].race_modifier = skill.race_modifiers[race]
