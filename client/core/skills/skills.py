# -*- coding: utf-8 -*-

from client.core.races import arach, xemna, grogg, harpy
from client.core.skills.skill_group import Skill_Group
from client.core.skills.skill import Skill

charisma = Skill(u'Charisma', u'the ability to influence other people',
    {arach: 0.4, grogg: 0.8, harpy: 1.8, xemna: 1.2}, ())
constitution = Skill(u'Constitution', u'physical endurance',
    {arach: 1.0, grogg: 1.6, harpy: 0.4, xemna: 1.2}, ())
determination = Skill(u'Determination', u'mental endurance',
    {arach: 1.2, grogg: 0.4, harpy: 1.2, xemna: 0.6}, ())
dexterity = Skill(u'Dexterity', u'the ability to move with precision',
    {arach: 1.2, grogg: 1.4, harpy: 0.8, xemna: 0.8}, ())
logic = Skill(u'Logic', u'mental power',
    {arach: 0.6, grogg: 0.6, harpy: 1.2, xemna: 1.2}, ())
perception = Skill(u'Perception', u'the ability to notice the things that should be noticed',
    {arach: 1.2, grogg: 0.8, harpy: 1.0, xemna: 1.2}, ())
reflex = Skill(u'Reflex', u'reaction speed',
    {arach: 1.2, grogg: 0.8, harpy: 1.2, xemna: 1.0}, ())
strength = Skill(u'Strength', u'physical power',
    {arach: 1.2, grogg: 1.6, harpy: 0.4, xemna: 0.8}, ())

primary_skills = (strength, constitution, reflex, dexterity, logic, determination, perception, charisma)

axe = Skill(u'Axe', u'the ability to fight with an axe', {}, (dexterity, strength))
blocking = Skill(u'Blocking', u'the ability to defend', {}, (perception, reflex, strength))
blunt = Skill(u'Blunt', u'the ability to fight with blunt weapons', {}, (dexterity, strength))
bow = Skill(u'Bow', u'the ability to fight with a bow', {}, (dexterity, determination))
dodging = Skill(u'Dodging', u'the ability to evade', {}, (dexterity, reflex))
spear = Skill(u'Spear', u'the ability to fight with a spear', {}, (dexterity, strength))
sword = Skill(u'Sword', u'the ability to fight with a sword', {}, (dexterity, strength))

fighting_skills = Skill_Group(u'Fighting skills', (axe, blocking, blunt, bow, dodging, spear, sword))

bartering = Skill(u'Bartering', u'the ability to get profitable prices in a trade',
    {}, (charisma, determination))
deception = Skill(u'Deception', u'the ability to decept other people',
    {}, (charisma, logic))
intimidation = Skill(u'Intimidation', u'the ability to intimidate other people',
    {}, (determination, strength))
leadership = Skill(u'Leadership', u'the ability to lead groups',
    {}, (charisma, determination))
manipulation = Skill(u'Manipulation', u'the ability to convince others of falcities',
    {}, (charisma, determination))

social_skills = Skill_Group(u'Social skills', (bartering, deception, intimidation, leadership, manipulation))

mechanics = Skill(u'Mechanics', u'the ability to use mechanical devices',
    {}, (logic, dexterity))
spotting = Skill(u'Spotting', u'the ability to spot traps, tracks and hidden items',
    {}, (perception,))
stealing = Skill(u'Stealing', u'the ability to take items without getting noticed',
    {}, (dexterity,))
stealth = Skill(u'Stealth', u'the ability to go unseen in the dark',
    {}, (dexterity, perception))

thieving_skills = Skill_Group(u'Thieving skills', (mechanics, spotting, stealing, stealth))
