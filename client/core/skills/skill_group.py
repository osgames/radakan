# -*- coding: utf-8 -*-

class Skill_Group(object):

    def __init__(self, name, members):

        super(Skill_Group, self).__init__()

        self.name = name
        self.members = members

    def __iter__(self):

        return self.members.__iter__()

    def __len__(self):

        return len(self.members)

    def __unicode__(self):

        return self.name
