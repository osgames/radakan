# -*- coding: utf-8 -*-

'''
This defines the L{Skill} class.
'''

from client.core.grammar import capitalize

class Skill(object):

    def __init__(self, name, description, race_modifiers, influencing_skills):

        self.__name = name
        self.__description = description
        self.__influencing_skills = influencing_skills
        self.__race_modifiers = race_modifiers

    def __unicode__(self):

        return self.__name

    @property
    def description(self):

        return capitalize(self.__name) + ' is ' + self.__description + '.'

    @property
    def influencing_skills(self):

        return self.__influencing_skills

    @property
    def race_modifiers(self):

        return self.__race_modifiers
