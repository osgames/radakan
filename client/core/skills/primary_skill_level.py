# -*- coding: utf-8 -*-

'''
This defines the L{Primary_Skill_Level} class.
'''

from client.core.skills.skill_level import Skill_Level

class Primary_Skill_Level(Skill_Level):

    def __init__(self, race_modifier):

        super(Primary_Skill_Level, self).__init__()

        self.points = 1.0
        self.race_modifier = float(race_modifier)

    @property
    def cap(self):

        return 1.5

    @property
    def giftedness(self):

        return (self.race_modifier * self.points) ** 0.5
