# -*- coding: utf-8 -*-

'''
This defines an L{execute} and a L{test} function.

This file has an underscore in its name to prevent having to use relative imports in this file of modules in the 'client' package. Panda3D's 'packp3d' can't handle relative imports.
'''

from client.user_interface.panda_window import Panda_Window

def execute():
    '''An Panda3D window is created and ran.'''

    return Panda_Window().run()
