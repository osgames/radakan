# -*- coding: utf-8-*-

try:
    from pandac.PandaModules import WindowProperties
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from common.halt_on_exception import halt_on_exception

from client.core.actions.rotate import Rotate
from client.core.actions.walk import Walk
from client.core.events import journey_state_change, mouse_view_change, screen_change
from client.core.items.characters import update_living_characters
from client.core.observer import Observer
from client.data.world_data import create_world
from client.user_interface.camera import Camera
from client.user_interface.head_up_display import Head_Up_Display
from client.user_interface.mouse import get_mouse_position, set_mouse_position
from client.user_interface.widgets.player_character_creation_screen import Player_Character_Creation_Screen
from client.user_interface.widgets.player_character_death_screen import Player_Character_Death_Screen

class Journey_Handler(Observer):
    '''The journey handler emits the corresponding screen_change when it receives a journey_state_change.'''

    def __init__(self):

        # We first end any running journeys. Note that this 'Journey_Handler' doesn't accept 'journey_state_change's yet.
        messenger.send(journey_state_change, [journey_state_change.end])

        super(Journey_Handler, self).__init__()

        self.__mouse_view = False

        self.__keys_down = set()
        self.__mouse_buttons_down = set()

        self.__last_x = float(0)
        self.__last_y = float(0)

        self.__camera = Camera()

        self.__update_world_prevtime = 0

        self.__player_character_creation_screen = Player_Character_Creation_Screen()
        self.__player_character_death_screen = Player_Character_Death_Screen()

        create_world()

        self.accept(journey_state_change, self.__handle_journey_state_change)

        self.__head_up_display = None
        self.__terminated = False

        messenger.send(screen_change, [screen_change.player_character_creation_screen])

    @property
    def terminated(self):

        return self.__terminated

    def __toggle_mouse_view(self):

        self.__mouse_view = not self.__mouse_view

        props = WindowProperties()
        props.setCursorHidden(self.__mouse_view)
        base.win.requestProperties(props)

        if self.__mouse_view:
            x, y = get_mouse_position()

            self.__last_x = x
            self.__last_y = y
        else:
            set_mouse_position(0, 0)

        messenger.send(mouse_view_change, [self.__mouse_view])

    def __handle_journey_state_change(self, state):

        print state

        if state == journey_state_change.run:

            self.__head_up_display = Head_Up_Display()

            taskMgr.add(self.__update_world_task, 'update world task')

            self.accept('escape', lambda: messenger.send(journey_state_change, [journey_state_change.pause]))

            # We use 'discard' instead of 'remove', because the key is not always present.
            self.accept('arrow_left', lambda : self.__start_turning('left'))
            self.accept('arrow_left-up', lambda : self.__stop_turning('left'))
            self.accept('arrow_right', lambda : self.__start_turning('right'))
            self.accept('arrow_right-up', lambda : self.__stop_turning('right'))
            self.accept('arrow_up', lambda : self.__start_moving('forward'))
            self.accept('arrow_up-up', lambda : self.__stop_moving('forward'))
            self.accept('arrow_down', lambda : self.__start_moving('backward'))
            self.accept('arrow_down-up', lambda : self.__stop_moving('backward'))
            self.accept('page_up', lambda : self.__keys_down.add('up'))
            self.accept('page_up-up', lambda : self.__keys_down.discard('up'))
            self.accept('page_down', lambda : self.__keys_down.add('down'))
            self.accept('page_down-up', lambda : self.__keys_down.discard('down'))

            self.accept('mouse1', self.__camera.go_to)
            self.accept('mouse2', self.__toggle_mouse_view)
            self.accept('mouse2-up', self.__update_turning)
            self.accept('mouse3', self.__camera.select_target)

            screen = screen_change.head_up_display

        else:

            # Ignore all mouse and keyboard input.
            self.ignoreAll()
            self.accept(journey_state_change, self.__handle_journey_state_change)

            # This is always allowed, even when these tasks aren't performed yet.
            taskMgr.remove('highlight item task')
            taskMgr.remove('update world task')

            if self.__head_up_display != None:
                if self.__mouse_view:
                    self.__toggle_mouse_view()

                # Destroying the HUD might trigger a 'Mouse_Over_Frame_Change' event.
                # So we'll set 'self.__head_up_display' to 'None', before destroying it,
                # to prevent wrong processing the 'Mouse_Over_Frame_Change' event.
                head_up_display = self.__head_up_display
                self.__head_up_display = None
                head_up_display.destroy()

            if state == journey_state_change.player_character_death:

                screen = screen_change.player_character_death_screen

            else:

                screen = screen_change.menu_screen

                if state == journey_state_change.end:

                    self.ignoreAll()

                    render.node().removeAllChildren()

                    self.__player_character_creation_screen.destroy()
                    self.__player_character_death_screen.destroy()

                    self.__terminated = True

        messenger.send(screen_change, [screen])

    @halt_on_exception
    def __start_moving(self, direction):

        self.__keys_down.add(direction)

        self.__update_movement()

    @halt_on_exception
    def __start_turning(self, direction):

        self.__keys_down.add(direction)

        self.__update_turning()

    @halt_on_exception
    def __stop_moving(self, direction):

        # We use 'discard' instead of 'remove', because the key is not always present.
        self.__keys_down.discard(direction)

        self.__update_movement()

    @halt_on_exception
    def __stop_turning(self, direction):

        # We use 'discard' instead of 'remove', because the key is not always present.
        self.__keys_down.discard(direction)

        self.__update_turning()

    @halt_on_exception
    def __update_movement(self):

        move_forward = 'forward' in self.__keys_down
        move_backward = 'backward' in self.__keys_down

        if move_forward and not move_backward:
            player_character.manual_movement = Walk(player_character, direction=1)
        elif move_backward and not move_forward:
            player_character.manual_movement = Walk(player_character, direction=- 1)
        else:
            player_character.manual_movement = None

    @halt_on_exception
    def __update_turning(self):

        turn_left = 'left' in self.__keys_down
        turn_right = 'right' in self.__keys_down

        if turn_left and not turn_right:
            player_character.manual_rotation = Rotate(actor=player_character, item=player_character, angle=- 90)
        elif turn_right and not turn_left:
            player_character.manual_rotation = Rotate(actor=player_character, item=player_character, angle=90)
        else:
            player_character.manual_rotation = None

    @halt_on_exception
    def __handle_camera_angle_keys(self, elapsed_time):

        if 'up' in self.__keys_down:
            self.__camera.look_angle +=  2 * elapsed_time
        if 'down' in self.__keys_down:
            self.__camera.look_angle -=  2 * elapsed_time

    @halt_on_exception
    def __handle_mouse_view(self, elapsed_time):

        assert self.__mouse_view, u'Mouse view is not active.'

        x, y = get_mouse_position()

        if x == self.__last_x:
            self.__update_turning()
        else:
            player_character.manual_rotation = Rotate(actor=player_character, item=player_character, angle=500 * (x - self.__last_x))

        self.__camera.look_angle +=  25 * elapsed_time * (y - self.__last_y)

        if 0.45 < max(abs(x), abs(y)):
            set_mouse_position(0, 0)
            self.__last_x = 0.0
            self.__last_y = 0.0
        else:
            self.__last_x = x
            self.__last_y = y

    @halt_on_exception
    def __update_world_task(self, task):

        # Limit the elapsed time to 0.1 second.
        elapsed = min(0.1, task.time - self.__update_world_prevtime)
        self.__update_world_prevtime = task.time

        self.__handle_camera_angle_keys(elapsed)

        if self.__mouse_view:
            self.__handle_mouse_view(elapsed)

        update_living_characters(elapsed)

        return task.cont
