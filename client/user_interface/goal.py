# -*- coding: utf-8 -*-

from math import pi, sin

try:
    from panda3d.core import CardMaker, NodePath, Point3, TransparencyAttrib
except ImportError, import_error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % import_error

from common.halt_on_exception import halt_on_exception

from client.core.actions.approach import Approach
from client.core.events import journey_state_change, player_character_action_change
from client.core.items.item import Item
from client.core.observer import Observer
from client.core.vector3 import Vector3

class Goal(Observer):

    def __init__(self):

        super(Observer, self).__init__()

        self.__destination = None

        card_maker = CardMaker('goal')
        card_maker.setColor(1, 0, 0, 1)
        card_maker.setFrame(Point3(- 1.5, - 1.5, 0.01), Point3(1.5, - 1.5, 0.01), Point3(1.5, 1.5, 0.01), Point3(- 1.5, 1.5, 0.01))

        self.__goal = render.attachNewNode(card_maker.generate())
        self.__goal.setTexture(loader.loadTexture('client/art/models/textures/circle.png'))
        self.__goal.setTransparency(TransparencyAttrib.MAlpha)
        self.__goal.setTwoSided(True)
        self.__goal.hide()

        self.accept(journey_state_change, self.__handle_journey_state_change)
        self.accept(player_character_action_change, self.__handle_player_character_action_change)

    def __handle_journey_state_change(self, state):

        if (player_character == None or not player_character.alive):

            self.ignoreAll()

    def __handle_player_character_action_change(self):

        if isinstance(player_character.planned_action, Approach):

            destination = player_character.planned_action.destination

            if isinstance(destination, Item):
                self.__goal.reparentTo(destination.model)
                self.__goal.setPos(Vector3(0, 0, 0))
            else:
                self.__goal.reparentTo(render)
                self.__goal.setPos(destination.coordinates)

            self.__goal.show()

        else:

            self.__goal.hide()
