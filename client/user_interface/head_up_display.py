# -*- coding: utf-8 -*-

from common.percentage import percentage

from client.core.actions.open import Open
from client.core.events import journey_state_change, hovered_item_change, mouse_view_change, proceeding, screen_change
from client.core.grammar import capitalize, object_
from client.user_interface.background import Background, floating_button_background, semi_transparent_background, transparent_background
from client.user_interface.mouse import get_mouse_position
from client.user_interface.widgets.body_widget import Body_Widget
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.context_menu import Context_Menu
from client.user_interface.widgets.frame import bottom_center, center, Frame, get_window_excentricity, top_center
from client.user_interface.widgets.inventory import Inventory
from client.user_interface.widgets.label import Label
from client.user_interface.widgets.log import Log
from client.user_interface.widgets.statistics_frame import Statistics_Frame
from client.user_interface.widgets.screen import Screen
from client.user_interface.widgets.text_frame import left

class Head_Up_Display(Screen):

    def __init__(self):

        assert player_character != None

        super(Head_Up_Display, self).__init__(
            screen_change.head_up_display
        )

        self.__previous_time = 0

        self.__target_label = None

        self.__player_character_widget = Body_Widget(player_character)
        #self.__target_widget = Body_Widget(None) # TODO

        self.__inventory = Inventory(player_character)
        self.__inventory.hide()
        self.__inventory.toggle_buttons.add(Button(
            command=self.__inventory.toggle_visibility,
            parent=top_center,
            text='Inventory',
            left=- 0.5,
            top=0,
            width=0.3
        ))

        self.__statistics = Statistics_Frame()
        self.__statistics.hide()
        self.__statistics.toggle_buttons.add(Button(
            command=self.__statistics.toggle_visibility,
            parent=top_center,
            text='Statistics',
            left=- 0.15,
            top=0,
            width=0.3
        ))

        self.__menu_button = Button(
            command=lambda: messenger.send(journey_state_change, [journey_state_change.pause]),
            parent=top_center,
            text='Menu',
            left=0.2,
            top=0,
            width=0.3
        )

        self.__log = Log(background=semi_transparent_background)

        self.__action_label = Label(
            background=semi_transparent_background,
            parent=bottom_center,
            text=u' ',
            text_align=left,
            left=- 1,
            bottom=0,
            width=2
        )

        self.__stop_button = Button(
            command=self.__stop_action,
            parent=bottom_center,
            text=u'Stop',
            left=0.8,
            bottom=0,
            width=0.2
        )
        self.__stop_button.hide()

        self.__hovered_item_label = Label(
            parent=center,
            text=u' ',
            left=0,
            bottom=0,
            width=0.2
        )

        self.__context_menu = Context_Menu()

        self.__crosshair = Frame(
            background=Background(texture='client/art/gui/crosshair.png'),
            parent=center
        )
        self.__crosshair.hide()

        self.accept(hovered_item_change, self.__handle_hovered_item_change)
        self.accept(mouse_view_change, self.__handle_mouse_view_change)
        self.accept(proceeding, self.__handle_proceeding)

    def destroy(self):

        self.ignoreAll()

        self.__crosshair.destroy()
        self.__player_character_widget.destroy()
        self.__inventory.destroy()
        self.__statistics.destroy()
        self.__menu_button.destroy()
        self.__log.destroy()
        self.__action_label.destroy()
        self.__stop_button.destroy()
        self.__hovered_item_label.destroy()
        self.__context_menu.destroy()

    def __handle_hovered_item_change(self, item):

        if item == None:
            self.__hovered_item_label.hide()

        else:
            x, y = 0, 0

            if self.__crosshair.isHidden():
                x, y = get_mouse_position()
                excentricity = get_window_excentricity(scaled=True)
                x *= excentricity[0]
                y *= excentricity[1]

            self.__hovered_item_label.show()
            self.__hovered_item_label.setProp('text', capitalize(object_(item)))
            self.__hovered_item_label.setProp('text_pos', (x, y + 0.05, 1))
            self.__hovered_item_label.resetFrameSize()

    def __handle_mouse_view_change(self, mouse_view):

        if mouse_view:
            self.__crosshair.show()
        else:
            self.__crosshair.hide()

    def __handle_proceeding(self, action):

        if isinstance(action, Open):

            Inventory(action.item).show() # Update the frame content.

        if player_character != None:

            planned_action = player_character.planned_action
            if planned_action == None:
                self.__stop_button.hide()

                self.__action_label.setProp('text', u'')
            else:
                self.__stop_button.show()

                text = planned_action.long_command
                if planned_action.to_do != planned_action:
                    text += u' ~ %s' % planned_action.to_do.long_command
                if 0 < planned_action.to_do.completion:
                    text += u' ~ %s completed.' % percentage(planned_action.to_do.completion)
                self.__action_label.setProp('text', text)

    def __stop_action(self):

        # I will notice and adapt the HUD.
        player_character.planned_action = None
