# -*- coding: utf-8-*-

import __builtin__

__builtin__.messenger = None
__builtin__.player_character = None

from webbrowser import open as open_web

from client.core.configuration_options import play_music, show_frame_rate, window_left, window_width, window_top, window_height

options = (
    'background-color 0.6 0.7 0.8',
    'framebuffer-multisample 1', # for anti-aliasing
    'multisamples 2', # for anti-aliasing
    'win-origin %s %s' % (window_left.value, window_top.value),
    'win-size %s %s' % (window_width.value, window_height.value),
    'window-title Radakan'
)

try:
    from panda3d.core import loadPrcFileData
    loadPrcFileData('', '\n'.join(options))

    from direct.showbase.ShowBase import ShowBase
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from common.halt_on_exception import halt_on_exception

# For 'client.core.configuration_options', see the top of this file.
from client.core.events import journey_state_change, screen_change
from client.core.models.model import prepare_collision_handling
from client.core.models.model_blueprint import set_app_runner_model_path
from client.core.observer import Observer
from client.user_interface.music import set_app_runner_music_path, toggle_music
from client.user_interface.journey_handler import Journey_Handler
from client.user_interface.widgets.background_frame import Background_Frame
from client.user_interface.widgets.configuration_screen import Configuration_Screen
from client.user_interface.widgets.credits import Credits
from client.user_interface.widgets.frame import apply_gui_scale
from client.user_interface.widgets.menu import Menu

# for debugging
#from panda3d.core import PStatClient
#PStatClient.connect()

class Panda_Window(Observer):

    def __init__(self):

        super(Panda_Window, self).__init__()

        self.__base = ShowBase()
        self.__base.disableMouse()

        # This is a workaround for the Panda3D build system.
        if self.__base.appRunner != None:
            set_app_runner_music_path(self.__base.appRunner.multifileRoot)
            set_app_runner_model_path(self.__base.appRunner.multifileRoot)

        apply_gui_scale()

        # Create the backgroudn first, to make sure that it is displayed behind the screens.
        self.__background = Background_Frame()

        self.__configuration_screen = Configuration_Screen()
        self.__credits = Credits()
        self.__journey_handler = None

        self.__menu = Menu(
            (u'Start new journey', self.__start_new_journey, lambda: (True, None)),
            (u'Continue journey', lambda: messenger.send(journey_state_change, [journey_state_change.run]), lambda: (False, u'There\'s no journey to continue.') if self.__journey_handler == None else (True, None)),
            (u'Save journey', self.__save_journey, lambda: (False, u'Saving and loading has not been implemented yet.')),
            (u'Load journey', self.__load_journey, lambda: (False, u'Saving and loading has not been implemented yet.')),
            (u'Settings', self.__show_configuration_screen, lambda: (True, None)),
            (u'About', self.__show_about, lambda: (False, u'This would open an the Radakan website (http://radakan.net/) in your browser, but it\'s currently bugged.')),
            (u'Credits', self.__show_credits, lambda: (True, None)),
            (u'Quit', self.__quit, lambda: (True, None))
        )

        self.__background.set_screens((screen_change.configuration_screen, screen_change.credits_screen, screen_change.menu_screen))

        if play_music.value:

            toggle_music()

        base.setFrameRateMeter(show_frame_rate.value)

        self.__window_border = None

        self.accept('aspectRatioChanged', self.__adapt_on_resize)
        self.accept('window-event', self.__adapt_on_move)

        prepare_collision_handling()

        self.accept(screen_change, self.__handle_screen_change)

        messenger.send(screen_change, [screen_change.menu_screen])

    @halt_on_exception
    def run(self):

        self.__base.run()

    @halt_on_exception
    def __adapt_on_resize(self, * args):

        window_width.raw_value = base.win.getXSize()
        window_height.raw_value = base.win.getYSize()

        apply_gui_scale()

    @halt_on_exception
    def __adapt_on_move(self, * args):

        # The window position isn't consistenly implemented in Panda3D.
        # See http://www.panda3d.org/phpbb2/viewtopic.php?t=9293 .
        # 'win-origin', 'window_left' and 'window_top' refer to the position of the inner window.
        # 'getXOrigin' and 'getYOrigin' refer to the position of the outer window.

        window_properties = base.win.getProperties()

        if self.__window_border == None: # only the first time

            self.__window_border = (window_properties.getXOrigin() - window_left.value, window_properties.getYOrigin() - window_top.value)

        window_left.raw_value = window_properties.getXOrigin() - self.__window_border[0]
        window_top.raw_value = window_properties.getYOrigin() - self.__window_border[1]

    @halt_on_exception
    def __handle_screen_change(self, screen_change_state):

        if self.__journey_handler != None:
            if self.__journey_handler.terminated:
                self.__journey_handler = None

    @halt_on_exception
    def __load_journey(self):

        messenger.send(game_state_change, [screen_change.loading_screen])

        # TODO

    @halt_on_exception
    def __save_journey(self):

        messenger.send(game_state_change, [screen_change.saving_screen])

        # TODO

    @halt_on_exception
    def __start_new_journey(self):

        self.__journey_handler = Journey_Handler()

    @halt_on_exception
    def __show_about(self):

        open_web('http://radakan.net/', autoraise=True)

    @halt_on_exception
    def __show_credits(self):

        self.__menu.hide()
        self.__credits.show()

    @halt_on_exception
    def __show_configuration_screen(self):

        messenger.send(screen_change, [screen_change.configuration_screen])

    @halt_on_exception
    def __quit(self):

        self.__base.taskMgr.stop()
