# -*- coding: utf-8 -*-

from math import pi, sin

try:
    from panda3d.core import AmbientLight, AntialiasAttrib, CollisionHandlerQueue, CollisionNode, CollisionRay, DirectionalLight, Vec4
except ImportError, import_error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % import_error

from common.halt_on_exception import halt_on_exception

from client.core.actions.approach import Approach
from client.core.events import journey_state_change, hovered_item_change, mouse_view_change, mouse_over_frame_change
from client.core.models.model import empty_mask, target_selection_mask
from client.core.observer import Observer
from client.core.vector3 import Vector3
from client.user_interface.goal import Goal
from client.user_interface.mouse import get_mouse_position
from client.user_interface.widgets.frame import is_mouse_over_frame

class Camera(Observer):

    def __init__(self):

        super(Camera, self).__init__()

        self.__hovered_item = None

        self.__look_angle = 0

        self.__mouse_view = False

        # This is necessary when all child nodes of the render have been destroyed.
        base.camera.reparentTo(render)

        # We set up the camera like it will be set up after character creation.
        base.camera.setPos(- 10.3505, - 14.1071, 1.8)
        base.camera.setHpr(- 29.9571, 0, 0)

        base.camLens.setFov(60) # A broader field of view gives greater depth.
        base.camLens.setNear(0.01)

        self.__directional_light_node_path = render.attachNewNode(DirectionalLight('directional light'))
        self.__directional_light_node_path.node().setColor(Vec4(0.7, 0.7, 0.7, 1))
        self.__directional_light_node_path.setHpr(0, - 60, 0)

        self.__ambient_light_node_path = render.attachNewNode(AmbientLight('ambient light'))
        self.__ambient_light_node_path.node().setColor(Vec4(0.3, 0.3, 0.3, 1))

        render.setLight(self.__directional_light_node_path)
        render.setShaderInput('light', self.__directional_light_node_path)
        render.setLight(self.__ambient_light_node_path)

        render.setShaderAuto()
        render.setAntialias(AntialiasAttrib.MAuto)

        self.__focus_light_node_path = render.attachNewNode(AmbientLight('focus light'))
        # The color will be set before using the focus light.

        self.__target_selection_ray = CollisionRay()

        self.__target_selection_node_path = camera.attachNewNode(CollisionNode('target selection node'))
        self.__target_selection_node_path.node().addSolid(self.__target_selection_ray)
        self.__target_selection_node_path.node().setFromCollideMask(target_selection_mask)
        self.__target_selection_node_path.node().setIntoCollideMask(empty_mask)
        #self.__target_selection_node_path.show()

        self.__target_selection_handler = CollisionHandlerQueue()

        base.cTrav.addCollider(self.__target_selection_node_path, self.__target_selection_handler)

        self.__goal_ray = CollisionRay()

        self.__goal_node_path = camera.attachNewNode(CollisionNode('goal node'))
        self.__goal_node_path.node().addSolid(self.__goal_ray)
        self.__goal_node_path.node().setFromCollideMask(target_selection_mask)
        self.__goal_node_path.node().setIntoCollideMask(empty_mask)
        #self.__goal_node_path.show()

        self.__goal_handler = CollisionHandlerQueue()

        base.cTrav.addCollider(self.__goal_node_path, self.__goal_handler)

        self.accept(journey_state_change, self.__handle_journey_state_change)
        self.accept(mouse_over_frame_change, self.__handle_mouse_over_frame_change)
        self.accept(mouse_view_change, self.__handle_mouse_view_change)

        self.__goal = Goal()

    @property
    def look_angle(self):

        return self.__look_angle

    @look_angle.setter
    @halt_on_exception
    def look_angle(self, new_look_angle):

        self.__look_angle = min(max(new_look_angle, - pi / 2), pi / 2)

        base.camera.lookAt(0, - 1000, 1.8 + 1000 * self.__look_angle)

    @halt_on_exception
    def go_to(self):
        '''This method will get called every time the mouse is left-clicked.'''

        if not is_mouse_over_frame():

            # Is the mouse inside the window?
            if base.mouseWatcherNode.hasMouse():

                x, y = 0, 0

                if not self.__mouse_view:

                    x, y = get_mouse_position()

                # Set the position of the ray based on the mouse position.
                self.__goal_ray.setFromLens(base.camNode, x, y)

                # Force the collision traverser to update the collisions, to prevent ending up with the collisions before 'self.__goal_ray' was set correctly.
                base.cTrav.traverse(render)

                if 0 < self.__goal_handler.getNumEntries():
                    self.__goal_handler.sortEntries()

                    entry = self.__goal_handler.getEntry(0)

                    destination = entry.getIntoNodePath().getNetPythonTag('item')

                    if destination == None:
                        destination = Vector3(self.__goal_handler.getEntry(0).getSurfacePoint(render))

                    player_character.planned_action = Approach(player_character, destination=destination)
                else:
                    print 'no entries'
            else:
                print 'mouse outside window'
        else:
            print 'mouse over frame'

    @halt_on_exception
    def select_target(self):
        '''This method will get called every time the mouse is right-clicked.'''

        if not is_mouse_over_frame():
            try:
                mouse_item = self.__get_mouse_item()
                if mouse_item == player_character.target:
                    player_character.target = None
                else:
                    player_character.target = mouse_item
            except AttributeError:
                pass

    def __get_mouse_item(self):

        # Is the mouse inside the window?
        if base.mouseWatcherNode.hasMouse():

            x, y = 0, 0

            if not self.__mouse_view:

                x, y = get_mouse_position()

            # Set the position of the ray based on the mouse position.
            self.__target_selection_ray.setFromLens(base.camNode, x, y)

            if 0 < self.__target_selection_handler.getNumEntries():
                self.__target_selection_handler.sortEntries()
                item = self.__target_selection_handler.getEntry(0).getIntoNodePath().getNetPythonTag('item')
                if item !=  None:
                    if item.model_item != player_character:
                        return item

        return None

    def __handle_journey_state_change(self, state):

        if state == journey_state_change.end:

            print 'clearing light...'

            self.ignoreAll()

            render.clearLight()

        else:

            base.camera.setPos(- 0.75, 2, 1.8)
            base.camera.reparentTo(player_character.model)

            # Trigger the camera directing, with the current angle.
            self.look_angle = self.look_angle

    def __handle_mouse_view_change(self, mouse_view):

        self.__mouse_view = mouse_view

    def __handle_mouse_over_frame_change(self, mouse_over_frame):

        # Multiple tasks with the same name *are* allowed.
        taskMgr.remove('highlight item task')

        self.__unhighlight_item()

        if not mouse_over_frame:

            taskMgr.add(self.__highlight_item_task, 'highlight item task')

    @halt_on_exception
    def __highlight_item_task(self, task):
        '''This task should only be enabled when the mouse is not hovering over a frame.'''

        self.__unhighlight_item()

        # Is the mouse inside the Panda3D window?
        if base.mouseWatcherNode.hasMouse():
            self.__hovered_item = self.__get_mouse_item()
        else:
            self.__hovered_item = None

        if self.__hovered_item != None:
            self.__focus_light_node_path.node().setColor(Vec4(0.3, 0.3, 0.1, 1) * (0.65 + 0.35 * sin(5 * task.time)))
            self.__hovered_item.model.highlighting = self.__focus_light_node_path

        messenger.send(hovered_item_change, [self.__hovered_item])

        return task.cont

    def __unhighlight_item(self):

        if self.__hovered_item != None:
            self.__hovered_item.model.highlighting = None

            messenger.send(hovered_item_change, [None])
