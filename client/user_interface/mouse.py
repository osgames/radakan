# -*- coding: utf-8 -*-

'''Horizontal and vertical range: - 1 to 1'''

from client.core.configuration_options import window_width, window_height

def get_mouse_position():

    pointer = base.win.getPointer(0)

    x = 2.0 * pointer.getX() / window_width.value - 1
    y = 1 - 2.0 * pointer.getY() / window_height.value

    return x, y

def set_mouse_position(x, y):

    x = (x + 1.0) * window_width.value / 2
    y = (1.0 - y) * window_height.value / 2

    base.win.movePointer(0, int(x), int(y))
