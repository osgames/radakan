# -*- coding: utf-8 -*-

# Some common backgrounds are defined at the bottom of this file.

try:
    from direct.gui.DirectFrame import DirectFrame
    from direct.gui.DirectGuiGlobals import NORMAL, WITHIN, WITHOUT
    from panda3d.core import NodePath, Texture, TransparencyAttrib
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from common.halt_on_exception import halt_on_exception

class Background(object):

    def __init__(self, color=(1, 1, 1, 1), texture=None):

        super(Background, self).__init__()

        self.__color = color
        self.__texture = texture

    def __unicode__(self):

        return u'(color: %s, texture: %s)' % (self.__color, self.__texture)

    @property
    def color(self):

        return self.__color

    @color.setter
    @halt_on_exception
    def color(self, color):

        self.__color = color

    @property
    def texture(self):

        if isinstance(self.__texture, basestring):

            result = loader.loadTexture(self.__texture)
            result.setWrapU(Texture.WMClamp)
            result.setWrapV(Texture.WMClamp)

            return result

        else:

            return self.__texture

default_button_background = Background(texture=u'client/art/gui/default_button.png')
default_frame_background = Background(texture=u'client/art/gui/default_frame.jpeg')
floating_button_background = Background(texture=u'client/art/gui/floating_button.png')
semi_transparent_background = Background(texture=u'client/art/gui/default_frame.jpeg', color=(1, 1, 1, 0.5))
transparent_background = Background(color=(0, 0, 0, 0))
