# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectGuiGlobals import FLAT
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from common.percentage import percentage

from client.core.events import player_character_perception, world_update_exception
from client.user_interface.background import Background, transparent_background
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.frame import bottom_center, small_spacing
from client.user_interface.widgets.label import Label
from client.user_interface.widgets.scrolled_frame import Scrolled_Frame
from client.user_interface.widgets.text_frame import left

class Log(Scrolled_Frame):

    def __init__(self, ** kwargs):

        super(Log, self).__init__(
            content_height=0.05,
            parent=bottom_center,
            reserved_top=0.06,
            left=- 1,
            right=1,
            bottom=0.065,
            top=0.665,
            ** kwargs
        )

        self.__resize_button = Button(
            background=Background(texture=u'client/art/gui/resize_up.png'),
            parent=self,
            relief=FLAT,
            right=self.right,
            top=self.top
        )

        self.accept(player_character_perception, self.__handle_player_event)
        self.accept(world_update_exception, self.__handle_player_event)

    def destroy(self):

        self.ignoreAll()

        super(Log, self).destroy()

    def __handle_player_event(self, perception):

        text = unicode(perception)

        if hasattr(perception, 'completion') and  0 < perception.completion < 1:
            text += u' ~ %s completed.' % percentage(perception.completion)

        for line in text.split('\n'):

            Label(
                left=self.canvas_wrapper.left,
                top=self.get_next_content_top(),
                parent=self.canvas_wrapper,
                text=line,
                text_align=left
            )

        self.adapt_to_children(follow=True)

    def get_next_content_top(self):

        return self.canvas_wrapper.top - self.canvas_wrapper.getNumChildren() * (0.05 + small_spacing)
