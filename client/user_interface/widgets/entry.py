# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectEntry import DirectEntry
    from direct.gui.DirectGuiGlobals import SUNKEN
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from client.user_interface.background import default_button_background
from client.user_interface.widgets.text_frame import Text_Frame

class Entry(Text_Frame, DirectEntry):

    def __init__(self, background=default_button_background, relief=SUNKEN, ** kwargs):

        super(Entry, self).__init__(
            background=background,
            relief=relief,
            ** kwargs
        )
