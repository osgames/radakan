# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectGuiGlobals import B1PRESS, B1RELEASE, FLAT, RAISED, RMB, SUNKEN
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from common.halt_on_exception import halt_on_exception

from client.core.actions.take_all import Take_All
from client.core.events import recontainment
from client.core.grammar import capitalize, object_, possessive
from client.core.items.composite_item import Composite_Item
from client.core.items.container import Container
from client.user_interface.background import Background, transparent_background
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.check_button import Check_Button
from client.user_interface.widgets.frame import Frame
from client.user_interface.widgets.popup_frame import Popup_Frame
from client.user_interface.widgets.scrolled_frame import Scrolled_Frame

# light grey background: unmovable items
# dark grey background: movable items

class Inventory(Popup_Frame):

    dragged_item = None
    dropped_container = None

    def __init__(self, container):

        self.container = None

        super(Inventory, self).__init__(
            name=u'%s inventory' % capitalize(possessive(container)),
            resize=True,
            width=1.28,
            height=1.08
        )

        self.__rows = list()
        self.__items_to_buttons = dict()

        self.container = container

        self.__contents = None

        self.__contents_bottom_margin = 0.14

        self.__item_area = Button(
            parent=self,
            relief=SUNKEN,
            left=self.left + 0.04,
            right=self.right - 0.16,
            bottom=self.bottom + self.__contents_bottom_margin,
            top=self.top - 0.04
        )

        self.__control_buttons = set()
        self.__show_composition_items_button = None
        self.__show_subcontainer_contents_button = None

        self.accept(recontainment, self.__handle_recontainment)

    @property
    @halt_on_exception
    def __items_per_row(self):

        return int((self.width - 0.15) / (0.155) + 0.000001)

    @property
    @halt_on_exception
    def __current_row(self):

        if (len(self.__rows) == 0) or (self.__items_per_row <= self.__rows[- 1].number_of_items):
            self.__rows.append(Frame(
                background=transparent_background,
                parent=self.__contents.canvas_wrapper
            ))

            self.__rows[- 1].number_of_items = 0

        return self.__rows[- 1]

    def hide(self):

        super(Inventory, self).hide()

        if player_character != None:
            for item, button in self.__items_to_buttons.iteritems():
                if item == player_character.target:
                    player_character.target = None

    def destroy(self):

        self.ignoreAll()

        self.hide()

        super(Inventory, self).destroy()

    def resize(self, * args, ** kwargs):

        super(Inventory, self).resize(* args, ** kwargs)

        if self.container != None:
            self.__update_contents()

    def show(self):

        super(Inventory, self).show()

        self.__update_contents()

    def __drag(self, item):

        def wrapper(* args):

            Inventory.dragged_item = item

            self.__items_to_buttons[item].setProp('relief', SUNKEN)

        return wrapper

    def __drop(self, * args):

        item = Inventory.dragged_item
        container = Inventory.dropped_container

        if container != None:

            # The situation might have changed since the container was set, so we re-check.
            if len(tuple(item.get_recontaining_problems(container))) == 0:

                item.container = container

        Inventory.dragged_item = None
        Inventory.dropped_container = None

    def __get_select(self, item):

        def select():

            if item == player_character.target:
                player_character.target = None
            else:
                player_character.target = item

        return select

    def __handle_recontainment(self, item, source):

        if not self.isHidden():

            # An item might be added or removed.
            self.__update_contents()

    def __within(self, item):

        def wrapper(* args):

            if Inventory.dragged_item == None:

                # Highlight the items that can be dragged.
                if not item.locked:
                    try:
                        self.__items_to_buttons[item].setProp('relief', SUNKEN)
                    except KeyError:
                        self.__update_contents()

        return wrapper

    def __without(self, item):

        def wrapper(* args):

            # This method will also be called when destroying the item list.
            try:
                if Inventory.dragged_item == None:

                    # Undo the highlighting.
                    self.__items_to_buttons[item].setProp('relief', RAISED)
            except KeyError:
                # The button no longer exists, so we don't have to worry about its style.
                pass

        return wrapper

    def __within_container(self, container):

        def wrapper(* args):

            item = Inventory.dragged_item

            if item != None:

                Inventory.dropped_container = container

                if len(tuple(item.get_recontaining_problems(container))) == 0:
                    self.__items_to_buttons[container].setProp('relief', SUNKEN)

        return wrapper

    def __without_container(self, container):

        def wrapper(* args):

            if Inventory.dragged_item != None:

                if container != Inventory.dragged_item:
                    self.__items_to_buttons[container].setProp('relief', RAISED)

                Inventory.dropped_container = None

        return wrapper

    def __update_contents(self, not_used=None):

        self.__update_control_buttons()
        self.__update_items()

    def __update_control_buttons(self):

        if self.__show_composition_items_button == None:
            show_composition_items = True
        else:
            show_composition_items = self.__show_composition_items_button.checked

        if self.__show_subcontainer_contents_button == None:
            show_subcontainer_contents = True
        else:
            show_subcontainer_contents = self.__show_subcontainer_contents_button.checked

        for button in self.__control_buttons:
            button.destroy()

        self.__take_all_button = Button(
            command=self.__take_all,
            parent=self,
            left=self.left + 0.04,
            width=0.4,
            bottom=self.bottom + 0.04,
            text=u'Take all items'
        )
        if self.container.model_item == player_character:
            self.__take_all_button.hide()
        else:
            problems = tuple(Take_All(player_character, self.container).problems)
            self.__take_all_button.enabled = (len(problems) == 0)
            self.__take_all_button.tooltip_text = u'\n'.join(problems)

        self.__show_composition_items_button = Check_Button(
            checked=show_composition_items,
            command=self.__update_contents,
            parent=self,
            right=self.right - 0.74,
            width=0.51,
            bottom=self.bottom + 0.04,
            text='Show composition items'
        )

        self.__show_subcontainer_contents_button = Check_Button(
            checked=show_subcontainer_contents,
            command=self.__update_contents,
            parent=self,
            right=self.right - 0.04,
            width=0.58,
            bottom=self.bottom + 0.04,
            text='Show subcontainer contents'
        )

        #back_button = Button(
            #parent=self,
            #left=self.__show_composition_items_button.left,
            #width=0.1,
            #top=self.__show_composition_items_button.top - 0.1,
            #height=0.1
        #)
        #front_button = Button(
            ##background='/tmp/puzzle.png',
            #background='/tmp/px.png',
            ##background='/tmp/composition_items.png',
            #parent=back_button,
            #relief=FLAT,
            #left=back_button.left + 0.005,
            #width=back_button.width - 0.01,
            #top=back_button.top - 0.005,
            #height=back_button.height - 0.01
        #)

        self.__control_buttons = [self.__take_all_button, self.__show_composition_items_button, self.__show_subcontainer_contents_button]

    def __update_items(self):

        for button in self.__items_to_buttons.values():
            button.destroy()

        self.__items_to_buttons = dict()
        self.__rows = list()

        if self.__contents != None:
            self.__contents.destroy()

        vertical_offset = min(self.bottom + self.__contents_bottom_margin, 0.0)

        self.__contents = Scrolled_Frame(
            background=transparent_background,
            parent=self,
            content_height=0.155,
            left=self.left + 0.04,
            right=self.right - 0.04,
            bottom=self.bottom + self.__contents_bottom_margin - vertical_offset, # This gives a glitch when below 0. :-S
            top=self.top - 0.1 - vertical_offset
        )

        self.__contents.setPos(0, 0, + vertical_offset)

        self.__item_area.resize(
            left=self.__contents.left,
            right=self.__contents.right - 0.06,
            bottom=self.__contents.bottom + vertical_offset,
            top=self.__contents.top + vertical_offset
        )

        # TODO Make this faster.

        # The subitems of these items will be added to this list later.
        items = list(sorted(self.container, key=unicode))

        while 0 < len(items):

            item = items.pop(0)

            # We don't need 'world' here.
            location = u''.join(u'\nat %s' % object_(ancestor) for ancestor in tuple(item.container_ancestors)[:- 1])

            if (self.__show_subcontainer_contents_button.checked and isinstance(item, Container)) or isinstance(item.container, Composite_Item):

                # By reversing the list, we make sure that the alphabetically first subitem will end up first, because every subitem is inserted before all other items.
                for subitem in sorted(item, key=unicode, reverse=True):
                    items.insert(0, subitem)

            if self.__show_composition_items_button.checked or not isinstance(item.container, Composite_Item):

                # We need two frame textures over eachother, so we'll use two buttons.
                back_button = Button(
                    parent=self.__current_row,
                    left=self.__current_row.number_of_items * 0.155 + 0.01,
                    width=0.15,
                    top=self.__contents.canvas_wrapper.top - len(self.__rows) * 0.155 + 0.145,
                    height=0.15
                )
                front_button = Button(
                    command=self.__get_select(item),
                    commandButtons=(RMB,),
                    background=Background(texture=item.model.icon_path),
                    parent=back_button,
                    relief=FLAT,
                    left=back_button.left + 0.005,
                    width=back_button.width - 0.01,
                    top=back_button.top - 0.005,
                    height=back_button.height - 0.01,
                    tooltip_text=u'%s%s\n\n%s' % (capitalize(object_(item)), location, item.tooltip_text)
                )
                self.__items_to_buttons[item] = back_button

                front_button.bind(B1PRESS, self.__drag(item))
                front_button.bind(B1RELEASE, self.__drop)
                front_button.within_callbacks.append(self.__within(item))
                front_button.without_callbacks.append(self.__without(item))
                if isinstance(item, Container):
                    front_button.within_callbacks.append(self.__within_container(item))
                    front_button.without_callbacks.append(self.__without_container(item))

                self.__current_row.number_of_items += 1

        self.__contents.adapt_to_children()

    def __take_all(self):

        player_character.planned_action = Take_All(player_character, self.container)
