# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectSlider import DirectSlider
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from client.user_interface.background import default_button_background, floating_button_background, transparent_background
from client.user_interface.widgets.frame import border_width, Frame

class Slider(Frame, DirectSlider):

    def __init__(self, command, parent, background=None, ** kwargs):

        if background == None:
            if parent.background == transparent_background:
                background = floating_button_background
            else:
                background = default_button_background

        super(Slider, self).__init__(
            background=background,
            command=command,
            parent=parent,
            thumb_borderWidth=border_width,
            thumb_clickSound=None,
            thumb_frameTexture=background.texture,
            thumb_frameSize=(- 0.015, 0.015, - 0.03, 0.03),
            thumb_rolloverSound=None,
            ** kwargs
        )
