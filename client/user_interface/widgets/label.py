# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectLabel import DirectLabel
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from client.user_interface.background import transparent_background
from client.user_interface.widgets.text_frame import Text_Frame

class Label(DirectLabel, Text_Frame):

    def __init__(self, background=transparent_background, ** kwargs):

        DirectLabel.__init__(self)
        Text_Frame.__init__(self, background=background, ** kwargs)
