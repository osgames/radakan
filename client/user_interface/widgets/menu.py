# -*- coding: utf-8 -*-

from client.core.events import screen_change
from client.user_interface.background import transparent_background
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.frame import center, Frame
from client.user_interface.widgets.screen import Screen
from client.user_interface.widgets.tooltip import Tooltip

class Menu(Screen):

    def __init__(self, * options):

        super(Menu, self).__init__(
            screen_change.menu_screen
        )

        self.__buttons = set()

        for i, (name, command, callback) in enumerate(options):

            button = Button(
                command=command,
                parent=self,
                left=- 0.2,
                top=0.3 - i * 0.125,
                text=name,
                width=0.4
            )
            button.callback = callback
            self.__buttons.add(button)

        self.show()

    def show(self):

        super(Menu, self).show()

        for button in self.__buttons:

            button.enabled, tooltip_text = button.callback()
            if tooltip_text == None:
                button.tooltip = None
            else:
                button.tooltip = Tooltip(parent=button, text=tooltip_text)
