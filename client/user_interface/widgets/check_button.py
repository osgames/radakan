# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectCheckButton import DirectCheckButton
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from common.halt_on_exception import halt_on_exception

from client.core.configuration_options import gui_scale
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.frame import small_spacing
from client.user_interface.widgets.text_frame import right

class Check_Button(Button, DirectCheckButton):

    def __init__(self, checked, ** kwargs):

        super(Check_Button, self).__init__(
            text_align=right,
            ** kwargs
        )

        # The frameSize is slightly larger than expected.
        self.resize(
            self.left + small_spacing,
            self.right - small_spacing,
            self.bottom + small_spacing,
            self.top - small_spacing
        )

        self.indicator.setScale(0.04)
        self.indicator.setPos(self.left + 0.018, 0, self.bottom + 0.015)

        self.checked = checked

    @property
    def checked(self):

        return self["indicatorValue"]

    @checked.setter
    @halt_on_exception
    def checked(self, checked):

        if checked != self.checked:

            self["indicatorValue"] = checked
            self.setIndicatorValue()
