# -*- coding: utf-8 -*-

from client.core.events import journey_state_change, screen_change
from client.core.grammar import capitalize, subject
from client.user_interface.layout import Layout
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.label import Label
from client.user_interface.widgets.popup_frame import Popup_Frame
from client.user_interface.widgets.screen import Screen

class Player_Character_Death_Screen(Screen):

    def __init__(self):

        super(Player_Character_Death_Screen, self).__init__(
            screen_change.player_character_death_screen
        )

        layout = Layout(
            columns=(0.06, 0.2, 0.1, 0.2, 0.06),
            rows=(0.06, 0.06, 0.06, 0.04, 0.06, 0.06),
            centered=True
        )

        self.__popup_frame = Popup_Frame(
            on_close=self.__close,
            parent=self,
            name=u'Character death',
            resize=False,
            width=layout.width,
            height=layout.height
        )

        self.__label = Label(
            area=layout.get_area(1, 2, column_span=3),
            parent=self.__popup_frame,
            text=u' '
        )

        Button(
            area=layout.get_area(2, 4),
            command=self.__close,
            parent=self.__popup_frame,
            text='Ok',
        )

        self.hide()

    def show(self):

        super(Player_Character_Death_Screen, self).show()

        self.__label['text'] = u'%s died.' % player_character

    def __close(self):

        messenger.send(journey_state_change, [journey_state_change.end])
