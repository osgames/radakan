# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectScrolledFrame import DirectScrolledFrame
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from client.user_interface.background import default_button_background, transparent_background
from client.user_interface.widgets.button import disabled_text_color
from client.user_interface.widgets.frame import border_width, default_frame_side, Frame, small_spacing
from client.user_interface.widgets.text_frame import text_color, text_scale

class Scrolled_Frame(Frame, DirectScrolledFrame):

    def __init__(self, content_height, left, right, bottom, top, reserved_top=0, ** kwargs):

        super(Scrolled_Frame, self).__init__(
            canvasSize=(0, right - left - default_frame_side, 0, top - bottom),

            horizontalScroll_frameSize=(0, 0, 0, 0), # Hide it.

            verticalScroll_command=self.__adapt_to_position,
            verticalScroll_frameTexture=default_button_background.texture,
            verticalScroll_frameSize=(right - default_frame_side, right, bottom, top - reserved_top),

            borderWidth=border_width,
            manageScrollBars=False,
            autoHideScrollBars=False,
            left=left,
            right=right,
            bottom=bottom,
            top=top,
            ** kwargs
        )

        self.__content_height = content_height

        self.verticalScroll.decButton.setProp('clickSound', None)
        self.verticalScroll.decButton.setProp('frameTexture', default_button_background.texture)
        self.verticalScroll.decButton.setProp('rolloverSound', None)
        self.verticalScroll.decButton.setProp('text', '/\\')
        self.verticalScroll.decButton.setProp('text_fg', text_color)
        self.verticalScroll.decButton.setProp('text_pos', (0, - default_frame_side / 6))
        self.verticalScroll.decButton.setProp('text_scale', text_scale)
        self.verticalScroll.decButton.setText()

        self.verticalScroll.thumb.setProp('clickSound', None)
        self.verticalScroll.thumb.setProp('frameTexture', default_button_background.texture)
        self.verticalScroll.thumb.setProp('rolloverSound', None)

        self.verticalScroll.incButton.setProp('clickSound', None)
        self.verticalScroll.incButton.setProp('frameTexture', default_button_background.texture)
        self.verticalScroll.incButton.setProp('rolloverSound', None)
        self.verticalScroll.incButton.setProp('text', '\\/')
        self.verticalScroll.incButton.setProp('text_fg', text_color)
        self.verticalScroll.incButton.setProp('text_pos', (0, - default_frame_side / 6))
        self.verticalScroll.incButton.setProp('text_scale', text_scale)
        self.verticalScroll.incButton.setText()

        self.canvas_wrapper = Frame(
            background=transparent_background,
            parent=self.canvas,
            left=0,
            top=self['canvasSize'][3]
        )

        self.__adapt_to_position()

    def adapt_to_children(self, follow=False):

        children_height = self.canvas_wrapper.getNumChildren() * (self.__content_height + small_spacing)

        if self['canvasSize'][3] < children_height + self['canvasSize'][2]:

            self.setProp('canvasSize', (
                0,
                self['canvasSize'][1],
                0,
                children_height
            ))

            self.canvas_wrapper.setPos(0, 0, children_height - self.canvas_wrapper.top)

        if follow:

            self.verticalScroll['value'] = 1

        self.__adapt_to_position()

    def __adapt_to_position(self):

        if self['canvasSize'][3] - self['canvasSize'][2] <= self.height:

            self.verticalScroll.decButton.setProp('text_fg', disabled_text_color)
            self.verticalScroll.incButton.setProp('text_fg', disabled_text_color)

        else:

            if self.verticalScroll['value'] == 0:
                self.verticalScroll.decButton.setProp('text_fg', disabled_text_color)
            else:
                self.verticalScroll.decButton.setProp('text_fg', text_color)

            if self.verticalScroll['value'] == 1:
                self.verticalScroll.incButton.setProp('text_fg', disabled_text_color)
            else:
                self.verticalScroll.incButton.setProp('text_fg', text_color)
