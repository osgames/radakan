# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectButton import DirectButton
    from pandac.PandaModules import MouseButton
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from common.halt_on_exception import halt_on_exception

from client.user_interface.background import default_button_background, floating_button_background, transparent_background
from client.user_interface.widgets.frame import Frame
from client.user_interface.widgets.text_frame import Text_Frame, text_color

disabled_text_color = (0.2, 0.2, 0.2, 1)
highlighted_text_color = (1, 0.5, 0, 1)

class Button(Text_Frame, DirectButton):

    def __init__(self, parent, background=None, command=(lambda * args: None), enabled=True, highlighted=False, tooltip_text=None, ** kwargs):

        if background == None:
            #if is_tooltip or # TODO
            if not isinstance(parent, Frame) or parent.background == transparent_background:
                background = floating_button_background
            else:
                background = default_button_background

        super(Button, self).__init__(
            background=background,
            command=self.__onclick_command,
            parent=parent,
            ** kwargs
        )

        self.__command = command
        self.__enabled = True
        self.__highlighted = False

        if tooltip_text == None or tooltip_text == u'':
            self.__tooltip = None
        else:
            from client.user_interface.widgets.tooltip import Tooltip

            self.__tooltip = Tooltip(parent=self, text=tooltip_text)

        # The tooltip may be set later.
        self.within_callbacks.append(self.__show_tooltip)
        self.without_callbacks.append(self.__hide_tooltip)

        self.setProp('clickSound', None)
        self.setProp('rolloverSound', None)

        self.enabled = enabled
        if enabled:
            self.highlighted = highlighted

    @property
    def enabled(self):
        '''
        Default: 'True'
        '''

        return self.__enabled

    @enabled.setter
    @halt_on_exception
    def enabled(self, enabled):
        '''Disabling a highlighted button will unhighlight it.'''

        # Do not unnecessarily overwrite, as it may remove highlighting.
        if enabled != self.enabled:

            if enabled:
                self.guiItem.addClickButton(MouseButton.one())

                self['text_fg'] = text_color
            else:
                self.guiItem.removeClickButton(MouseButton.one())

                self['text_fg'] = disabled_text_color

            self.__enabled = enabled

    @property
    def highlighted(self):
        '''
        Default: 'False'
        '''

        return self.__highlighted

    @highlighted.setter
    @halt_on_exception
    def highlighted(self, highlighted):
        '''Only highlight enabled buttons.'''

        assert self.enabled, u'A disabled button can\'t be highlighted.'

        self.__highlighted = highlighted

        if highlighted:
            self['text_fg'] = highlighted_text_color
        else:
            # Make sure the next step will paint the default color.
            self.enabled = False # This has a side effect of unhighlighting the button.
            # Paint the default color.
            self.enabled = True

    @property
    def tooltip(self):

        return self.__tooltip

    @tooltip.setter
    @halt_on_exception
    def tooltip(self, new_tooltip):

        if self.__tooltip != None:
            self.__tooltip.destroy()

        self.__tooltip = new_tooltip

    def __onclick_command(self, * args):

        if self.enabled:
            self.__command(* args)

    def __show_tooltip(self):

        if self.tooltip != None:
            self.__tooltip.show()

    def __hide_tooltip(self):

        if self.tooltip != None:
            self.__tooltip.hide()
