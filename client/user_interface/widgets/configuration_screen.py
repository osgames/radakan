# -*- coding: utf-8 -*-

from client.core.configuration_options import gui_scale, play_music, point_of_view, show_frame_rate
from client.core.events import screen_change
from client.core.grammar import capitalize, conjugate, possessive, subject
from client.user_interface.background import floating_button_background, transparent_background
from client.user_interface.music import toggle_music
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.frame import apply_gui_scale, center, Frame
from client.user_interface.widgets.label import Label
from client.user_interface.widgets.screen import Screen
from client.user_interface.widgets.slider import Slider

def _get_point_of_view_tooltip_text(pov):

    name = '<name>'
    if player_character != None:
        name = player_character

    return u'For example: \"%s %s a knive in %s backpack.\"' % (capitalize(subject(name, pov=pov, pc=True)), conjugate('place', None, pov=pov, pc=True), possessive(name, pov=pov, pc=True))

class Configuration_Screen(Screen):

    def __init__(self):

        super(Configuration_Screen, self).__init__(screen_change.configuration_screen)

        if show_frame_rate.value:
            frame_rate_button_text = u'Hide frame rate'
        else:
            frame_rate_button_text =  u'Show frame rate'

        self.__frame_rate_button = Button(
            command=self.__toggle_frame_rate,
            parent=self,
            text=frame_rate_button_text,
            width=0.4,
            top=0.35
        )

        if play_music.value:
            music_button_text = u'Stop music'
        else:
            music_button_text =  u'Play music'

        self.__music_button = Button(
            command=self.__toggle_music,
            parent=self,
            text=music_button_text,
            width=0.4,
            top=self.__frame_rate_button.bottom - 0.06
        )

        label = Label(
            parent=self,
            text='Point of view:',
            width=0.4,
            top=self.__music_button.bottom - 0.06
        )

        self.__point_of_view_buttons = list()

        self.__point_of_view_buttons.append(Button(
            command=self.__set_point_of_view(1),
            highlighted=(point_of_view.value == 1),
            parent=self,
            text='First person',
            tooltip_text=_get_point_of_view_tooltip_text(1),
            width=0.4,
            top=label.bottom - 0.005
        ))

        self.__point_of_view_buttons.append(Button(
            command=self.__set_point_of_view(2),
            highlighted=(point_of_view.value == 2),
            parent=self,
            text='Second person',
            tooltip_text=_get_point_of_view_tooltip_text(2),
            width=0.4,
            top=self.__point_of_view_buttons[- 1].bottom - 0.005
        ))

        self.__point_of_view_buttons.append(Button(
            command=self.__set_point_of_view(3),
            highlighted=(point_of_view.value == 3),
            parent=self,
            text='Third person',
            tooltip_text=_get_point_of_view_tooltip_text(3),
            width=0.4,
            top=self.__point_of_view_buttons[- 1].bottom - 0.005
        ))

        label = Label(
            parent=self,
            text='GUI scale:',
            width=0.4,
            top=self.__point_of_view_buttons[- 1].bottom - 0.05
        )

        self.__slider = Slider(
            command=self.__set_gui_scale,
            parent=self,
            range=gui_scale.range,
            value=gui_scale.raw_value,
            width=0.4,
            top=label.bottom - 0.005
        )

        Button(
            command=lambda: messenger.send(screen_change, [screen_change.menu_screen]),
            parent=self,
            text='Back',
            width=0.4,
            top=self.__slider.bottom - 0.06
        )

        self.accept('f', self.__toggle_frame_rate)
        self.accept('m', self.__toggle_music)

    def __set_gui_scale(self):

        gui_scale.raw_value = self.__slider['value']

        apply_gui_scale()

    def __set_point_of_view(self, new_point_of_view):

        def wrapper():

            point_of_view.raw_value = new_point_of_view

            for index, button in enumerate(self.__point_of_view_buttons):
                button.highlighted = (index + 1 == new_point_of_view)

        return wrapper

    def __toggle_music(self):

        # toggle
        play_music.raw_value = not play_music.value

        if self.__music_button != None:
            if play_music.value:
                self.__music_button.setProp('text', u'Stop music')
            else:
                self.__music_button.setProp('text', u'Play music')

        toggle_music()

    def __toggle_frame_rate(self):

        # toggle
        show_frame_rate.raw_value = not show_frame_rate.value

        if self.__frame_rate_button != None:
            if show_frame_rate.value:
                self.__frame_rate_button.setProp('text', u'Hide frame rate')
            else:
                self.__frame_rate_button.setProp('text', u'Show frame rate')

        base.setFrameRateMeter(show_frame_rate.value)
