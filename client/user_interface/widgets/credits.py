# -*- coding: utf-8 -*-

from client.core.events import screen_change
from client.user_interface.background import floating_button_background, transparent_background
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.frame import center
from client.user_interface.widgets.label import Label
from client.user_interface.widgets.screen import Screen

class Credits(Screen):

    def __init__(self):

        super(Credits, self).__init__(screen_change.credits_screen)

        Label(
            parent=self,
            bottom=0.3,
            text='''
Many thanks to everyone who helped creating Radakan:

3ShadowD, Akinwale, Animefanatic, Ascii, Axelius, Bb0x, benalene, Blue-palm,
Brandoxpirate, Clauchiorean, Cosmin, CrimsonFox, Deathknight2005, Digilante,
Ex-Machina, Echorev, Emil, Erebusnight, Erixxn, Fix, Garkudion, Hanhisuanto,
Hasta84, Hellmr, Ignatius, Ilidrake, InfernoZeus, Ivar, J!mb022, Jenkis,
Kangaroo, Kenshee, Lasse, LeartS, LonelyArtist, Markus, Medice, Mensis,
Metallon, Mike, Momoko-Fan, NruJaC, Oakie, Prominence, qubodup, remaxim, R-s-g,
Ri0ku, Ridley, Sadr, Satsuki, Scott, SSShvb, Taldor, Tariqwalji, Thaliost,
Thellis, Trapdoor, TriX3, Umbral, Vogdush, Weastmann, Welchcompositions,
wikikiwi, Wildex999, Yanic and everyone else who was accidentically omitted.
            '''
        )

        Button(
            command=lambda: messenger.send(screen_change, [screen_change.menu_screen]),
            parent=self,
            text='Back',
            width=0.4,
            bottom=- 0.3
        )
