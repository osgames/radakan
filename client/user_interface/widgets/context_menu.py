# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectGuiGlobals import FLAT
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from common.halt_on_exception import halt_on_exception

from client.core.actions.approach import Approach
from client.core.actions.consume import Consume
from client.core.actions.damage import Damage
from client.core.actions.destroy import Destroy
from client.core.actions.drop import Drop
from client.core.actions.equip import Equip
from client.core.actions.open import Open
from client.core.actions.take import Take
from client.core.events import player_character_action_change, player_character_target_change, proceeding
from client.user_interface.background import transparent_background
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.inside_frame import Inside_Frame
from client.user_interface.widgets.text_frame import left

class Context_Menu(Inside_Frame):

    __static_action_types = (
        (Approach, Open),
        (Damage, Destroy, Consume),
        (Take, Equip, Drop)
    )

    def __init__(self):

        self.__action_buttons = None

        super(Context_Menu, self).__init__(
            background=transparent_background,
            width=0.61,
            top=0.065,
            sortOrder=200
        )

        self.__target_label = Button(
            parent=self,
            relief=FLAT,
            text=u' ',
            # Use the full width of the context menu so the text is perfectly centered.
            width=self.width,
            top=self.top
        )

        Button(
            background=transparent_background,
            command=self.__close,
            parent=self,
            text=u'X',
            # Let it overlap with the target label to make sure that the target label text is properly centered.
            right=self.right,
            top=self.top
        )

        self.__action_buttons = set()

        self.hide()

    @property
    def bottom(self):

        if self.__action_buttons == None or len(self.__action_buttons) == 0:
            return super(Context_Menu, self).bottom

        else:
            return min(button.bottom for button in self.__action_buttons)

    def destroy(self):

        self.ignoreAll()

        super(Context_Menu, self).destroy()

    def hide(self):

        self.ignoreAll()
        self.accept(player_character_action_change, self.__repopulate)
        self.accept(player_character_target_change, self.__repopulate)

        for button in self.__action_buttons:
            button.destroy()
        self.__action_buttons = set()

        super(Context_Menu, self).hide()

    def __close(self):

        player_character.target = None

        self.hide()

    def __handle_proceeding(self, action):

        self.__update_position()

        if action.interesting and (player_character.target in action.involved_items):

            self.__repopulate()

    @halt_on_exception
    def __open(self):

        self.show()

        self.__target_label.setProp('text', unicode(player_character.target))
        self.__target_label.tooltip_text = player_character.target.tooltip_text

        for y, action_types in enumerate(Context_Menu.__static_action_types):

            for x, action_type in enumerate(action_types):

                # Problems might not be obvious to the player. So the button is always shown when the action type applies, even if the action itself is impossible.
                if action_type.does_apply(player_character):
                    action = action_type(player_character)

                    problems = list(action.problems)
                    if 1 <= action.completion:
                        problems.insert(0, u'This is already done.')

                    button = Button(
                        command=self.__plan_action(action),
                        enabled=(len(problems) == 0),
                        parent=self,
                        text=action.short_command,
                        text_align=left,
                        tooltip_text=u'\n'.join(problems),
                        left=x * 0.205 - 0.305,
                        top=- 0.065 * y,
                        width=0.2
                    )
                    button.action = action
                    self.__action_buttons.add(button)

        dynamic_actions = player_character.target.dynamic_player_character_actions

        for i, action in enumerate(sorted(dynamic_actions, key=lambda action: action.short_command)):

            button = Button(
                command=self.__plan_action(action),
                parent=self,
                text=action.short_command,
                text_align=left,
                left=- 0.305,
                top=- 0.13 - 0.065 * i,
                width=0.61
            )
            button.action = action
            self.__action_buttons.add(button)

        if player_character.planned_action != None:
            short_command = player_character.planned_action.short_command

            for button in self.__action_buttons:
                if button.enabled:
                    button.highlighted = (button['text'] == short_command)

        self.__update_position()

        self.accept(proceeding, self.__handle_proceeding)

    def __plan_action(self, action):

        def wrapper():

            player_character.planned_action = action

            player_character.target = None

        return wrapper

    def __repopulate(self):

        self.hide()

        if player_character.target != None:
            self.__open()

    def __update_position(self):

        if player_character.target != None:

            x, y = 0, 0
            if 0 < len(player_character.target.gui_widgets):
                widget = player_character.target.gui_widgets[0]
                x = (widget.left + widget.right) / 2
                y = widget.bottom - 0.07
            else:
                x, y = player_character.target.model_item.model.center_2d

            self.setPos(x, 0, y)
