# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectGuiGlobals import B1PRESS, FLAT, RAISED
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from client.user_interface.background import Background, transparent_background
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.frame import get_window_excentricity
from client.user_interface.widgets.inside_frame import Inside_Frame

class Popup_Frame(Inside_Frame):

    def __init__(self, name, resize, on_close=None, ** kwargs):

        super(Popup_Frame, self).__init__(
            relief=RAISED,
            ** kwargs
        )

        self.__moving_offset_x = None
        self.__moving_offset_y = None
        self.__resizing_offset = None

        self.__on_close = on_close

        # For the top area we'll use a plain button, hidden behind another transparent button, to make sure the former isn't pressed.
        self.__top_bar = Button(
            parent=self,
            text=name,
            left=self.left,
            width=self.width,
            top=self.top
        )

        self.__move_button = Button(
            background=transparent_background,
            parent=self,
            left=self.left,
            width=self.width,
            top=self.top
        )
        self.__move_button.bind(B1PRESS, self.__start_moving)

        self.__close_button = Button(
            background=transparent_background,
            command=self.__close,
            parent=self,
            right=self.right,
            top=self.top,
            text='X'
        )

        if resize:
            self.__resize_button = Button(
                background=Background(texture=u'client/art/gui/resize.png'),
                parent=self,
                relief=FLAT,
                right=self.right,
                bottom=self.bottom
            )
            self.__resize_button.bind(B1PRESS, self.__start_resizing)
        else:
            self.__resize_button = None

    def resize(self, * args, ** kwargs):

        super(Popup_Frame, self).resize(* args, ** kwargs)

        if hasattr(self, '_Popup_Frame__top_bar'):

            self.__top_bar.resize(self.left, self.right, self.top - 0.06, self.top)

            self.__move_button.resize(area=self.__top_bar)

            self.__close_button.resize(self.right - 0.06, self.right, self.top - 0.06, self.top)

            self.__resize_button.resize(self.right - 0.06, self.right, self.bottom, self.bottom + 0.06)

    def __close(self):

        self.hide()

        if self.__on_close != None:

            self.__on_close()

    def __move_task(self, task):

        if base.mouseWatcherNode.hasMouse():

            excentricity = get_window_excentricity(scaled=True)

            # We make use of the 'setPos' method of 'Inside_Frame', which will make sure that the frame stays inside the visible area.
            self.setPos(
                (self.__moving_offset_x + base.mouseWatcherNode.getMouseX()) * excentricity[0],
                0,
                (self.__moving_offset_y + base.mouseWatcherNode.getMouseY()) * excentricity[1]
            )

        return task.cont

    def __resize_task(self, task):

        if base.mouseWatcherNode.hasMouse():

            mouse_position = base.mouseWatcherNode.getMouse()
            excentricity = get_window_excentricity(scaled=True)
            self.resize(
                right=mouse_position.getX() * excentricity[0] + self.__resizing_offset[0],
                bottom=mouse_position.getY() * excentricity[1] + self.__resizing_offset[1]
            )

        return task.cont

    def __start_moving(self, * not_used):

        self.__moving_offset_x, not_used, self.__moving_offset_y = self.getPos(render2d)
        self.__moving_offset_x -= base.mouseWatcherNode.getMouseX()
        self.__moving_offset_y -= base.mouseWatcherNode.getMouseY()

        taskMgr.add(self.__move_task, 'move task for %s' % self)

        self.acceptOnce('mouse1-up', self.__stop_moving)

    def __start_resizing(self, * not_used):

        if base.mouseWatcherNode.hasMouse():

            mouse_position = base.mouseWatcherNode.getMouse()
            excentricity = get_window_excentricity(scaled=True)
            self.__resizing_offset = (
                self.right - mouse_position.getX() * excentricity[0],
                self.bottom - mouse_position.getY() * excentricity[1]
            )

            taskMgr.add(self.__resize_task, 'resize task for %s' % self)

            self.acceptOnce('mouse1-up', self.__stop_resizing)

    def __stop_moving(self, * not_used):

        taskMgr.remove('move task for %s' % self)

        self.__moving_offset_x = None
        self.__moving_offset_y = None

    def __stop_resizing(self):

        taskMgr.remove('resize task for %s' % self)
