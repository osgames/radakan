# -*- coding: utf-8 -*-

from client.core.events import screen_change
from client.user_interface.background import transparent_background
from client.user_interface.widgets.frame import center, Frame

class Screen(Frame):

    def __init__(self, screen_change_state):

        super(Screen, self).__init__(
            background=transparent_background,
            parent=center
        )

        self.__screen_change_state = screen_change_state

        self.hide()

        self.accept(screen_change, self.__handle_screen_change)

    def __handle_screen_change(self, screen_change_state):

        assert screen_change_state in screen_change, u'%s should be one of %s.' % (screen_change_state, screen_change)

        if screen_change_state == self.__screen_change_state:
            self.show()
        else:
            self.hide()
