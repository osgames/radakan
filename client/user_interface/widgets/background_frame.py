# -*- coding: utf-8 -*-

from client.core.events import screen_change
from client.user_interface.background import Background
from client.user_interface.widgets.frame import Frame, get_window_excentricity

class Background_Frame(Frame):

    def __init__(self):

        super(Background_Frame, self).__init__(
            background=Background(color=(0.2, 0.2, 0.2, 1)), # the dark grey color on the website
            parent=None,
            sortOrder=- 1000,
        )

        self.__image_frame = None
        self.__screens = None

        self.__draw()

        self.accept('aspectRatioChanged', self.__draw)
        self.accept(screen_change, self.__handle_screen_change)

    def __draw(self, * args):

        if self.__image_frame != None:
            self.__image_frame.destroy()

        excentricity = get_window_excentricity(scaled=False)

        self.resize(- excentricity[0], excentricity[0], - excentricity[1], excentricity[1])

        image_ratio = float(480) / 1440

        self.__image_frame = Frame(
            background=Background(texture='client/art/gui/background_top_mh.png'),
            parent=self,
            sortOrder=- 900,
            width=2 * excentricity[0],
            top=excentricity[1],
            height=2 * excentricity[0] * image_ratio
        )

    def set_screens(self, screens):

        self.__screens = screens

    def __handle_screen_change(self, screen):

        print screen, self.__screens

        if screen in self.__screens:
            self.show()
        else:
            self.hide()
