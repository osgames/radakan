# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectGuiGlobals import FLAT
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from common.percentage import percentage

from client.core.events import condition_change
from client.user_interface.background import Background, transparent_background
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.frame import top_left, Frame
from client.user_interface.widgets.label import Label

def _get_color(condition):

    red = None
    green = None

    if condition <= 0:

        red = 0.3
        green = 0

    elif condition < 0.5:

        red = 1
        green = 2 * condition

    else:

        red = 2 - 2 * condition
        green = 1

    return (red, green, 0, 1)

class Body_Widget(Frame):

    def __init__(self, character):

        super(Body_Widget, self).__init__(parent=top_left, background=transparent_background)

        # This is a front view, so the right side of the body is near the left screen border.

        left = 0.03
        top = - 0.03

        scale = 0.0008 # screen areas per pixel
        width = 334 * scale
        height = 853 * scale

        path = u'client/art/gui/xemna_%s.png'

        self.__character = character

        self.__widgets = dict()

        self.__widgets[self.__character.head] = Button(
            parent=self,
            background=Background(color=_get_color(1), texture=path % 'head'),
            left=left,
            width=width,
            top=top,
            height=height,
            relief=FLAT
        )

        self.__widgets[self.__character.chest] = Button(
            parent=self,
            background=Background(color=_get_color(1), texture=path % 'body'),
            left=left,
            width=width,
            top=top,
            height=height,
            relief=FLAT
        )

        self.__widgets[self.__character.left_arm] = Button(
            parent=self,
            background=Background(color=_get_color(1), texture=path % 'left_arm'),
            left=left,
            width=width,
            top=top,
            height=height,
            relief=FLAT
        )

        self.__widgets[self.__character.right_arm] = Button(
            parent=self,
            background=Background(color=_get_color(1), texture=path % 'right_arm'),
            left=left,
            width=width,
            top=top,
            height=height,
            relief=FLAT
        )

        self.__widgets[self.__character.left_leg] = Button(
            parent=self,
            background=Background(color=_get_color(1), texture=path % 'left_leg'),
            left=left,
            width=width,
            top=top,
            height=height,
            relief=FLAT
        )

        self.__widgets[self.__character.right_leg] = Button(
            background=Background(color=_get_color(1), texture=path % 'right_leg'),
            parent=self,
            left=left,
            width=width,
            top=top,
            height=height,
            relief=FLAT
        )

        self.__text = Label(
            parent=self,
            left=width + left,
            width=width,
            top=top
            )

        self.__set_text()

        self.accept(condition_change, self.__handle_condition_change)

    def destroy(self):

        self.ignoreAll()

        super(Body_Widget, self).destroy()

    def __handle_condition_change(self, item):

        if item in self.__widgets.keys():

            background = self.__widgets[item].background
            background.color = _get_color(item.condition)
            self.__widgets[item].background = background

        self.__set_text()

    def __set_text(self):

        text = u''

        for body_part in self.__character:

            if body_part.condition < 1:

                text += u'%s: %s\n' % (body_part, percentage(body_part.condition))

        self.__text.setProp('text', text)
