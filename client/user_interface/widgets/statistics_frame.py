# -*- coding: utf-8 -*-

from common.percentage import percentage

from client.core.events import tracker_change
from client.core.grammar import capitalize
from client.core.skills.skills import primary_skills
from client.core.tracker import trackers
from client.user_interface.widgets.label import Label
from client.user_interface.widgets.popup_frame import Popup_Frame
from client.user_interface.widgets.text_frame import left, right

class Statistics_Frame(Popup_Frame):

    def __init__(self):

        super(Statistics_Frame, self).__init__(
            name=u'Statistics',
            resize=False,
            width=0.72,
            height=1.005
        )

        self.__name_label = Label(
            parent=self,
            left=self.left + 0.04,
            top=self.top - 0.1,
            text=unicode(player_character),
            text_align=left,
            width=0.8
        )

        self.__gender_race_label = Label(
            parent=self,
            left=self.left + 0.04,
            top=self.__name_label.top - 0.05,
            text=u'%s %s' % (capitalize(player_character.gender), player_character.race),
            text_align=left,
            width=0.2
        )

        self.__skill_percentages = dict()
        for i, skill in enumerate(sorted(primary_skills, key=unicode)):

            top = self.__gender_race_label.top - 0.1 - 0.05 * i

            Label(
                parent=self,
                left=self.left + 0.04,
                top=top,
                text=unicode(skill),
                text_align=left,
                width=0.3
            )

            skill_level = getattr(player_character, str(unicode(skill)).lower())
            self.__skill_percentages[skill_level] = Label(
                parent=self,
                text_align=right,
                right=self.right - 0.04,
                top=top,
                width=0.1
            )

        Label(
            parent=self,
            left=self.left + 0.04,
            top=self.__gender_race_label.top - 0.55,
            text=u'Volume',
            text_align=left
        )

        self.__volume_label = Label(
            parent=self,
            right=self.right - 0.04,
            top=self.__gender_race_label.top - 0.55,
            text=u'%.1f jars' % player_character.size,
            text_align=right
        )

        Label(
            parent=self,
            left=self.left + 0.04,
            top=self.__volume_label.top - 0.05,
            text=u'Weight',
            text_align=left
        )

        self.__weight_label = Label(
            parent=self,
            right=self.right - 0.04,
            top=self.__volume_label.top - 0.05,
            text=u'%.1f fist' % player_character.weight,
            text_align=right
        )

        self.__tracker_labels = dict()

        for i, tracker in enumerate(trackers):
            label = Label(
                parent=self,
                left=self.left + 0.04,
                top=self.__weight_label.top - 0.1 - 0.05 * i,
                text=tracker.name,
                text_align=left
            )

            self.__tracker_labels[tracker] = Label(
                parent=self,
                right=self.right - 0.04,
                top=label.top,
                text_align=right
            )

        self.__adapt()

        self.accept(tracker_change, self.__adapt)

    def show(self):

        self.__adapt()

        super(Statistics_Frame, self).show()

    def __adapt(self, not_used=None):

        for skill_level, label in self.__skill_percentages.iteritems():
            label.setProp('text', percentage(skill_level))

        for tracker, label in self.__tracker_labels.iteritems():
            label.setProp('text', unicode(tracker.value))
