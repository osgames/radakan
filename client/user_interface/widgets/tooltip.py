# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectGuiGlobals import RIDGE
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from common.halt_on_exception import halt_on_exception

from client.user_interface.background import Background, floating_button_background
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.inside_frame import Inside_Frame
from client.user_interface.widgets.text_frame import left

class Tooltip(Button, Inside_Frame):

    def __init__(self, parent, text, text_align=left, relief=RIDGE, ** kwargs):

        super(Tooltip, self).__init__(
            # Altough a patch has been added for it, frameTextures with a RIDGE relief still didn't seem to work in a Panda3D build.
            #background=floating_button_background,
            background=Background(color=(0.4, 0.4, 0.4, 1)),
            parent=parent,
            relief=relief,
            sortOrder=500, # in front of all else
            text=text,
            text_align=left,
            left=parent.left + 0.065,
            top=parent.bottom - 0.005,
            ** kwargs
        )

        for component in self.components():
            self.component(component).setWordwrap(30)

        bounds = self.getBounds()

        self.resize(right=bounds[1] + 0.008, bottom=bounds[2] - 0.006)

        self.hide()
        self.setBin('gui-popup', 500) # in front of all else

    @property
    def text(self):

        return self['text']
