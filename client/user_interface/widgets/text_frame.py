# -*- coding: utf-8 -*-

try:
    from panda3d.core import TextNode
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from client.user_interface.widgets.frame import Frame, small_spacing

text_color = (0.9, 0.9, 0.9, 1)
text_scale = 0.04

left = TextNode.ALeft
center = TextNode.ACenter # default text alignment
right = TextNode.ARight

class Text_Frame(Frame):

    def __init__(self, text=u'', text_align=center, ** kwargs):

        super(Text_Frame, self).__init__(** kwargs)

        self.__text_align = text_align

        # The text should be set before setting its position, color and scale.
        self.setProp('text', text)
        self.setText()

        self.__set_text_position()
        self.setProp('text_align', text_align)

        self.setProp('text_fg', text_color)
        self.setProp('text_scale', text_scale)

    def resize(self, * args, ** kwargs):

        super(Text_Frame, self).resize(* args, ** kwargs)

        if self['text'] != None:
            self.__set_text_position()

    def __set_text_position(self):

        text_pos_x = None
        if self.__text_align == left:
            text_pos_x = self.left + 2.0 * small_spacing
        elif self.__text_align == right:
            text_pos_x = self.right - 2.0 * small_spacing
        else: # center
            text_pos_x = (self.left + self.right) / 2.0

        self.setProp('text_pos', (text_pos_x, self.top - 0.04))
