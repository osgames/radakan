# -*- coding: utf-8 -*-

from client.user_interface.widgets.frame import center, Frame, get_window_excentricity

class Inside_Frame(Frame):
    '''This will stay inside the visible area.'''

    def __init__(self, parent=center, ** kwargs):

        super(Inside_Frame, self).__init__(
            parent=parent,
            ** kwargs
        )

        self.accept('aspectRatioChanged', self.__adapt)

    def resize(self, * args, ** kwargs):

        super(Inside_Frame, self).resize(* args, ** kwargs)

        self.__adapt()

    def setPos(self, x, p, y):

        excentricity = get_window_excentricity(scaled=True)

        s_x, p, s_y = self.getPos() # This can differ slightly from the arguments.
        p_x, p, p_y = self.getPos(center)

        # Make sure that the position (x or y) + the parental offset (p_x - s_x or p_y - s_y) + the offset to the side of the frame (left, right, bottom, or top) do not exceed the distance from the center of the screen to the side of the screen (+/- excentricity).
        x = max(x, - excentricity[0] - p_x + s_x - self.left)
        x = min(x, + excentricity[0] - p_x + s_x - self.right)
        y = max(y, - excentricity[1] - p_y + s_y - self.bottom)
        y = min(y, + excentricity[1] - p_y + s_y - self.top)

        super(Inside_Frame, self).setPos(x, p, y)

    def show(self):

        super(Inside_Frame, self).show()

        self.__adapt()

    def __adapt(self, * args):

        self.setPos(* self.getPos())
