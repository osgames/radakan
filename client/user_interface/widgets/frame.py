# -*- coding: utf-8 -*-

try:
    from direct.gui.DirectFrame import DirectFrame
    from direct.gui.DirectGuiGlobals import NORMAL, WITHIN, WITHOUT
    from panda3d.core import NodePath, Texture, TransparencyAttrib
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from common.halt_on_exception import halt_on_exception

from client.core.configuration_options import gui_scale
from client.core.events import mouse_over_frame_change
from client.user_interface.background import default_frame_background, transparent_background

default_frame_side = 0.06
small_spacing = 0.005
border_width = (small_spacing, small_spacing)

top_left = NodePath('top_left')
top_center = NodePath('top_center')
top_right = NodePath('top_right')
center_left = NodePath('center_left')
center = NodePath('center')
center_right = NodePath('center_right')
bottom_left = NodePath('bottom_left')
bottom_center = NodePath('bottom_center')
bottom_right = NodePath('bottom_right')

_hovered_frames = set()

def apply_gui_scale():

    top_left.reparentTo(base.a2dTopLeft)
    top_left.setScale(gui_scale.value)

    top_center.reparentTo(base.a2dTopCenter)
    top_center.setScale(gui_scale.value)

    top_right.reparentTo(base.a2dTopRight)
    top_right.setScale(gui_scale.value)

    center_left.reparentTo(base.a2dLeftCenter)
    center_left.setScale(gui_scale.value)

    center.reparentTo(aspect2d)
    center.setScale(gui_scale.value)

    center_right.reparentTo(base.a2dRightCenter)
    center_right.setScale(gui_scale.value)

    bottom_left.reparentTo(base.a2dBottomLeft)
    bottom_left.setScale(gui_scale.value)

    bottom_center.reparentTo(base.a2dBottomCenter)
    bottom_center.setScale(gui_scale.value)

    bottom_right.reparentTo(base.a2dBottomRight)
    bottom_right.setScale(gui_scale.value)

def get_window_excentricity(scaled):
    '''The window excentricity may vary during the game.'''

    if scaled:
        return (base.a2dRight / gui_scale.value, base.a2dTop / gui_scale.value)
    else:
        return (base.a2dRight, base.a2dTop)

def is_mouse_over_frame():

    return 0 < len(_hovered_frames)


def _get_min_and_max(min_=None, max_=None, lenght=None):

    assert(None in (min_, max_, lenght))

    if None in (min_, max_):

        if lenght == None:
            lenght = default_frame_side

        if (min_ == None) and (max_ == None):
            # centered
            max_ = lenght / 2
            min_ = - max_
        elif min_ != None:
            max_ = min_ + lenght
        else:
            min_ = max_ - lenght

    return min_, max_

class Frame(DirectFrame):

    def __init__(self, background=default_frame_background, left=None, right=None, bottom=None, top=None, width=None, height=None, area=None,  ** kwargs):

        # To enable 'bind' for a DirectLabel, it's state should be set to 'NORMAL', because it's 'DISABLED' by default.
        super(Frame, self).__init__(state=NORMAL, suppressMouse=False, ** kwargs)

        if area == None:

            left, right = _get_min_and_max(left, right, width)
            bottom, top = _get_min_and_max(bottom, top, height)

            self.resize(left, right, bottom, top)

        else: # if area != None:

            assert(set([left, right, bottom, top, width, height]) == set([None]))

            self.resize(area=area)

        self.__background = None

        self.toggle_buttons = set()

        # See http://www.panda3d.org/phpbb2/viewtopic.php?t=2495 .
        self.initialiseoptions(type(self))

        self.setProp('borderWidth', border_width)

        self.background = background

        self.within_callbacks = list()
        self.without_callbacks = list()

        self.bind(WITHIN, self.__within)
        self.bind(WITHOUT, self.__without)

    @property
    def background(self):

        return self.__background

    @background.setter
    @halt_on_exception
    def background(self, background):

        self.__background = background

        if background.texture != None:
            self.setProp('frameTexture', background.texture)

        self.setTransparency(TransparencyAttrib.MAlpha)

        self.setProp('frameColor', background.color)

    @property
    def left(self):

        return self['frameSize'][0]

    @property
    def right(self):

        return self['frameSize'][1]

    @property
    def width(self):

        return self.right - self.left

    @property
    def bottom(self):

        return self['frameSize'][2]

    @property
    def top(self):

        return self['frameSize'][3]

    @property
    def height(self):

        return self.top - self.bottom

    def create_toggle_visibility_button(self, ** kwargs):

        # lazy import, because of circular dependency
        from client.user_interface.widgets.button import Button

        text = type(self).__name__.replace('_Frame', '')
        if 'text' in kwargs.keys():
            text = kwargs['text']
            del kwargs['text']

        self.toggle_buttons.add(Button(
            command=self.toggle_visibility,
            text=text,
            ** kwargs
        ))

    def destroy(self):

        self.__without()

        for button in self.toggle_buttons:
            button.destroy()

        DirectFrame.destroy(self)

    def hide(self):

        self.__without()

        for toggle_button in self.toggle_buttons:
            toggle_button.highlighted = False

        super(Frame, self).hide()

    def resize(self, left=None, right=None, bottom=None, top=None, area=None):

        if area == None:

            if left == None:
                left = self.left
            if right == None:
                right = self.right
            if bottom == None:
                bottom = self.bottom
            if top == None:
                top = self.top

            self.setProp('frameSize', (left, right, bottom, top))

        else: # if area != None:

            assert(set([left, right, bottom, top]) == set([None]))

            if hasattr(area, 'left'):
                self.resize(area.left, area.right, area.bottom, area.top)
            else:
                self.resize(* area)

    def show(self):

        for toggle_button in self.toggle_buttons:
            toggle_button.highlighted = True

        super(Frame, self).show()

    def toggle_visibility(self):

        # This might seem strange, but remember that we're toggling...
        visible = self.isHidden()

        if visible:
            self.show()
        else:
            self.hide()

    def __within(self, * args):

        # We prevent getting a lot of useless calls.
        if self.background != transparent_background:

            if len(_hovered_frames) == 0:
                messenger.send(mouse_over_frame_change, [True])

            _hovered_frames.add(self)

        for within_callback in self.within_callbacks:

            within_callback()

    def __without(self, * args):

        # We prevent getting a lot of useless calls.
        #if self.background != transparent_background: # This doesn't work for destoyed frames.

        try:
            _hovered_frames.remove(self)
        except KeyError:
            pass
        else:
            # 'elif' doesn't work here.
            if len(_hovered_frames) == 0:
                messenger.send(mouse_over_frame_change, [False])

        for without_callback in self.without_callbacks:

            without_callback()
