# -*- coding: utf-8 -*-

from client.core.events import journey_state_change, screen_change
from client.core.genders import genders, male
from client.core.grammar import capitalize
from client.core.races import races, xemna
from client.core.skills.skills import primary_skills
from client.core.skills.character_creation_skillset import Character_Creation_Skillset
from client.data.world_data import create_player_character
from client.user_interface.background import Background, transparent_background
from client.user_interface.layout import Layout
from client.user_interface.widgets.button import Button
from client.user_interface.widgets.entry import Entry
from client.user_interface.widgets.frame import default_frame_side, Frame
from client.user_interface.widgets.label import Label
from client.user_interface.widgets.popup_frame import Popup_Frame
from client.user_interface.widgets.screen import Screen
from client.user_interface.widgets.text_frame import left, right

class Player_Character_Creation_Screen(Screen):

    def __init__(self):

        layout = Layout(
            columns=(0.06, 0.2, 0.07, 0.3, 0.06, 0.06, 0.06, 0.06, 0.005, 0.06, 0.06),
            rows=(0.06, 0.06, 0.06, 0.07) + 8 * (0.06, 0.005) + (0.06, 0.07, 0.06, 0.06),
            centered=True
        )

        super(Player_Character_Creation_Screen, self).__init__(
            screen_change.player_character_creation_screen
        )

        self.__popup_frame = Popup_Frame(
            name=u'Character creation',
            on_close=lambda: messenger.send(journey_state_change, [journey_state_change.end]),
            parent=self,
            resize=False,
            width=layout.width,
            height=layout.height
        )

        #layout.draw(self) ###

        self.__gender = None
        self.__race = None

        self.__skillset = Character_Creation_Skillset(xemna)

        Label(
            area=layout.get_area(1, 2),
            parent=self.__popup_frame,
            text='Name',
            text_align=left
        )

        self.__name_input = Entry(
            area=layout.get_area(2, 2, column_span=8),
            parent=self.__popup_frame,
            text_align=left
        )
        self.__name_input.setup()
        self.__name_input.enterText(u'Taldor')

        self.__gender_buttons = dict()
        for i, gender in enumerate(sorted(genders)):

            self.__gender_buttons[gender] = Button(
                area=layout.get_area(1, 4 + 2 * i),
                command=self.__set_gender(gender),
                parent=self.__popup_frame,
                text=capitalize(gender)
            )

        self.__race_buttons = dict()
        for i, race in enumerate(sorted(races, key=unicode)):

            self.__race_buttons[race] = Button(
                area=layout.get_area(1, 10 + 2 * i),
                command=self.__set_race(race),
                parent=self.__popup_frame,
                text=capitalize(race),
                tooltip_text=race.description
            )

        self.__lower_skill_buttons = dict()
        self.__skill_points = dict()
        self.__raise_skill_buttons = dict()
        self.__skill_percentages = dict()
        for i, skill in enumerate(sorted(primary_skills, key=unicode)):

            # We use a button here to get a tooltip text.
            skill_name = Button(
                # Spans under all widgets for this skill.
                area=layout.get_area(3, 4 + 2 * i, column_span=5),

                background=transparent_background,
                parent=self.__popup_frame,
                text=unicode(skill),
                text_align=left,
                tooltip_text=skill.description
            )

            self.__lower_skill_buttons[skill] = Button(
                area=layout.get_area(4, 4 + 2 * i),
                command=self.__get_lower_skill(skill),
                enabled=False,
                parent=self.__popup_frame,
                text=u'-',
                # A button hides tooltips from the widgets underneath.
                tooltip_text=skill.description
            )
            # Make the tooltip appear at the same position as the tooltip of the skill name.
            self.__lower_skill_buttons[skill]._Button__tooltip.resize(
                area=skill_name._Button__tooltip
            )

            self.__skill_points[skill] = Label(
                area=layout.get_area(5, 4 + 2 * i),
                parent=self.__popup_frame,
                text=unicode(self.__skillset.get_skill_points(skill)),
                text_align=right
            )

            self.__raise_skill_buttons[skill] = Button(
                area=layout.get_area(6, 4 + 2 * i),
                command=self.__get_raise_skill(skill),
                parent=self.__popup_frame,
                text=u'+',
                # A button hides tooltips from the widgets underneath.
                tooltip_text=skill.description
            )
            # Make the tooltip appear at the same position as the tooltip of the skill name.
            self.__raise_skill_buttons[skill]._Button__tooltip.setProp('frameSize', skill_name._Button__tooltip['frameSize'])
            self.__raise_skill_buttons[skill]._Button__tooltip.setProp('text_pos', skill_name._Button__tooltip['text_pos'])

            self.__skill_percentages[skill] = Label(
                area=layout.get_area(7, 4 + 2 * i, column_span=3),
                parent=self.__popup_frame,
                text=self.__skillset.get_skill_percentage(skill),
                text_align=right,
            )

        Label(
            area=layout.get_area(3, 20),
            parent=self.__popup_frame,
            text='Skill points left',
            text_align=left
        )

        self.__skill_points_left = Label(
            area=layout.get_area(5, 20),
            parent=self.__popup_frame,
            text='%s' % self.__skillset.get_skill_points_left(),
            text_align=right
        )

        self.__all_minimum_button = Button(
            area=layout.get_area(7, 20),
            command=self.__reset_to_minima,
            parent=self.__popup_frame,
            tooltip_text=u'Set all skill points to the minimal value.',
            text='_'
        )

        for character in 'v|':
            Label(
                parent=self.__all_minimum_button,
                area=self.__all_minimum_button,
                text=character
            )

        self.__all_average_button = Button(
            area=layout.get_area(9, 20),
            command=self.__reset_to_averages,
            parent=self.__popup_frame,
            text='=',
            tooltip_text=u'Set all skill points to the average value.'
        )

        Button(
            command=self.__create_player_character,
            parent=self.__popup_frame,
            bottom=self.__popup_frame.bottom + 0.06,
            text='Begin the journey',
            width=0.4
        )

        self.__set_gender(male)()
        self.__set_race(xemna)()

    def __create_player_character(self):

        name = unicode(self.__name_input.get(True))
        if len(name) == 0:
            name = u'Taldor'

        create_player_character(
            name=name,
            gender=self.__gender,
            race=self.__race,
            skill_levels=self.__skillset.skill_levels
        )

        messenger.send(journey_state_change, [journey_state_change.run])

    def __get_lower_skill(self, skill):

        def lower_skill():

            assert self.__skillset.can_lower_skill(skill), u'Skill %s can\'t be lowered.'

            self.__skillset.lower_skill(skill)

            self.__load_skill_data()

        return lower_skill

    def __get_raise_skill(self, skill):

        def raise_skill():

            assert self.__skillset.can_raise_skill(skill), u'Skill %s can\'t be raised.'

            self.__skillset.raise_skill(skill)

            self.__load_skill_data()

        return raise_skill

    def __reset_to_minima(self):

        self.__skillset.reset(minima=True)

        self.__load_skill_data()

    def __reset_to_averages(self):

        self.__skillset.reset()

        self.__load_skill_data()

    def __set_gender(self, selected_gender):

        def result():
            self.__gender = selected_gender
            for gender, button in self.__gender_buttons.iteritems():
                if button.enabled:
                    button.highlighted = (gender == selected_gender)
                else:
                    assert gender != selected_gender, u'An unallowed gender is selected.'

        return result

    def __set_race(self, selected_race):

        def set_race():
            self.__race = selected_race
            self.__skillset.set_race(selected_race)

            for race, button in self.__race_buttons.iteritems():
                button.highlighted = (race == selected_race)

            for gender, button in self.__gender_buttons.iteritems():
                button.enabled = (gender in selected_race.genders)

            if self.__gender in selected_race.genders:
                self.__set_gender(self.__gender)()
            else:
                self.__set_gender(selected_race.genders[0])()

            self.__load_skill_data()

        return set_race

    def __load_skill_data(self):

        for skill, lower_button in self.__lower_skill_buttons.iteritems():
            lower_button.enabled = self.__skillset.can_lower_skill(skill)

        for skill, raise_button in self.__raise_skill_buttons.iteritems():
            raise_button.enabled = self.__skillset.can_raise_skill(skill)

        for skill, points in self.__skill_points.iteritems():
            points.setProp('text', unicode(self.__skillset.get_skill_points(skill)))

        for skill, percentage in self.__skill_percentages.iteritems():
            percentage.setProp('text', unicode(self.__skillset.get_skill_percentage(skill)))

        self.__all_minimum_button.enabled = (any(self.__skillset.get_skill_points(skill) != self.__skillset.minimum_primary_skill_points for skill in primary_skills))

        self.__all_average_button.enabled = (0 < self.__skillset.get_skill_points_left())

        self.__skill_points_left.setProp('text', str(self.__skillset.get_skill_points_left()))
