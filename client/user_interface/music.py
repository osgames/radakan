# -*- coding: utf-8 -*-

from os import listdir
from random import choice

try:
    from panda3d.core import AudioSound
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

__music = None
__music_path = u'client/art/music'

def set_app_runner_music_path(app_runner_path):

    global __music_path
    __music_path = u'%s/%s' % (app_runner_path, __music_path)

def get_music_status():

    return (__music !=  None) and (__music.status() ==  AudioSound.PLAYING)

def toggle_music():

    global __music

    if get_music_status():
        __music.stop()
    else:
        files = [file_ for file_ in listdir(u'%s/' % __music_path) if '.ogg' in file_]

        if len(files) ==  0:
            raise IOError, 'No music files found.'
        else:
            file_ = choice(files)
            __music = loader.loadSfx(u'%s/%s' % (__music_path, file_))
            __music.setLoopCount(0) # Loop forever.
            __music.play()
            print u'playing music: %s' % file_
