# -*- coding: utf-8 -*-

from math import fsum

class Layout(object):

    def __init__(self, columns, rows, centered=False):

        self.__column_borders = [fsum(columns[0:index]) for index in range(len(columns) + 1)]
        self.__row_borders = [- fsum(rows[0:index]) for index in range(len(rows) + 1)]

        if centered:
            self.__column_borders = [column_border - self.width / 2 for column_border in self.__column_borders]
            self.__row_borders = [row_border + self.height / 2 for row_border in self.__row_borders]

    @property
    def width(self):

        return self.__column_borders[- 1] - self.__column_borders[0]

    @property
    def height(self):

        return self.__row_borders[0] - self.__row_borders[- 1]

    def draw(self, parent):

        from client.user_interface.background import Background
        from client.user_interface.widgets.frame import Frame

        for column in range(len(self.__column_borders) - 1):
            for row in range(len(self.__row_borders) - 1):

                if 0 < (- 1) ** (column + row):
                    Frame(
                        area=self.get_area(column, row),
                        background=Background(color=(0.4, 0.4, 0.4, 0.5)),
                        parent=parent
                    )

    def get_area(self, column, row, column_span=1, row_span=1):

        return (
            self.__column_borders[column],
            self.__column_borders[column + column_span],
            self.__row_borders[row + row_span],
            self.__row_borders[row]
        )

    def get_row_position(self, row):

        return self.__row_borders[row]

    def get_column_position(self, column):

        return self.__column_borders[column]
