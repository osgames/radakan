# -*- coding: utf-8 -*-

from common.halt_on_exception import halt_on_exception

from client.core.actions.offer import Offer
from client.core.actions.speak import Speak
from client.core.items.non_player_character import how_are_you, Non_Player_Character
from client.data.flags import feargus_might_have_kamdaz_trumpet
from client.data.non_player_characters.feargus import give_it_back

get_lost = 'get lost'
_here_is_your_trumpet = u'here is your trumpet'

class Kamdaz(Non_Player_Character):

    def __init__(self, ** kwargs):

        super(Kamdaz, self).__init__(** kwargs)

        self.plan_reaction(1, Speak(
            self,
            content=u'Please master Feargus, give it back. Please!',
            target=self.container.find_one(lambda x: unicode(x) == u'Feargus'),
            tone=u'beg',
            topic=give_it_back
        ))

    def process_action(self, action):

        if isinstance(action, Speak) and (action.target == self):

            if action.topic == get_lost:
                if player_character != None:
                    # Set the target so the player character can respond without greeting first.
                    self.target = player_character
                    self.plan_reaction(1, Speak(
                        self,
                        content=u'Hey, you there! Can\'t you help me with getting my trumpet back?',
                        target=player_character,
                        tone=u'ask'
                    ))
                    return

            if action.topic == how_are_you:
                #if not self.i_have_the_offered_item: # TODO
                self.plan_reaction(1, Speak(
                    self,
                    content=u'Master Kamdaz stole my trumpet while I was asleep. He better gives it back quickly, or I\'ll...',
                    flags=(feargus_might_have_kamdaz_trumpet,),
                    target=action.actor
                ))
                return

        return super(Kamdaz, self).process_action(action)

    @property
    @halt_on_exception
    def dynamic_player_character_actions(self):

        dynamic_player_character_actions = super(Kamdaz, self).dynamic_player_character_actions

        if not isinstance(dynamic_player_character_actions, tuple):

            # Am I listening to the player character?
            if self.target == player_character:

                dynamic_player_character_actions.add(Speak(
                    player_character,
                    content=u'What\'s going on?',
                    target=self,
                    tone=u'ask',
                    topic=how_are_you
                ))

                trumpet = player_character.find_one(lambda x: u'trumpet' in unicode(x), recursive=True)
                if trumpet != None:

                    dynamic_player_character_actions.add(Offer(
                        player_character,
                        content='Here\'s your trumpet!',
                        item=trumpet,
                        target=self,
                        topic=_here_is_your_trumpet
                    ))

        return dynamic_player_character_actions
