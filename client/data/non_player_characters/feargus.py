# -*- coding: utf-8 -*-

from common.halt_on_exception import halt_on_exception

from client.core.actions.speak import Speak
from client.core.items.non_player_character import how_are_you, Non_Player_Character
from client.data.flags import feargus_might_have_kamdaz_trumpet

give_it_back = 'Give it back!'
_did_you_steal_kamdaz_trumpet = 'Did you steal Kamdaz\' trumpet?'

class Feargus(Non_Player_Character):

    def __init__(self, ** kwargs):

        super(Feargus, self).__init__(** kwargs)

    def process_action(self, action):

        if isinstance(action, Speak) and (action.target == self):

            if action.topic == give_it_back:
                # Do a lazy import to prevent an import cycle.
                from client.data.non_player_characters.kamdaz import get_lost

                self.plan_reaction(1, Speak(
                    self,
                    content=u'Quit the whining, and get back to work.',
                    target=action.actor,
                    topic=get_lost
                ))
                return

            if action.topic == how_are_you:
                if self.container.find_one(lambda x: unicode(x) == u'Kamdaz').alive:
                    self.plan_reaction(1, Speak(
                        self,
                        content=u'I\'m great! Feargus annoyed me a lot with that trumpet he found lately, but I took care of\nthat... He, he.',
                        target=action.actor
                    ))
                    return

            if action.topic == _did_you_steal_kamdaz_trumpet:
                self.plan_reaction(1, Speak(
                    self,
                    content=u'No, I didn\'t steal that old trumpet. I just threw it away.',
                    target=action.actor
                ))
                return

        super(Feargus, self).process_action(action)

    @property
    @halt_on_exception
    def dynamic_player_character_actions(self):

        dynamic_player_character_actions = super(Feargus, self).dynamic_player_character_actions

        if not isinstance(dynamic_player_character_actions, tuple):

            # Am I listening to the player character?
            if self.target == player_character:

                # Is the player character smart enough?
                if 0.9 < float(player_character.logic):

                    if feargus_might_have_kamdaz_trumpet in player_character.knowledge:

                        dynamic_player_character_actions.add(Speak(
                            player_character,
                            content='Did you steal Kamdaz\' trumpet?',
                            target=self,
                            tone=u'ask',
                            topic=_did_you_steal_kamdaz_trumpet
                        ))

        return dynamic_player_character_actions
