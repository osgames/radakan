# -*- coding: utf-8 -*-

try:
    from panda3d.core import CollisionSphere, CollisionPolygon
except ImportError, error:
    raise ImportError, 'Panda3D could not be found. Did you install it?\n%s' % error

from client.core.genders import male
from client.core.grammar import possessive
from client.core.items.container import Container
from client.core.items.player_character import Player_Character
from client.core.items.item import Item, consumable
from client.core.items.world import World
from client.core.models.model import Model
from client.core.models.model_blueprint import Model_Blueprint
from client.core.models.model_group import Model_Group
from client.core.races import xemna
from client.core.vector3 import Vector3
from client.data.non_player_characters.feargus import Feargus
from client.data.non_player_characters.kamdaz import Kamdaz

def __sign(x):

    return cmp(x, 0)

# terrain model blueprints:

ground_blueprint = Model_Blueprint(name=u'ground', scale=100, terrain=True)

field_blueprint = Model_Blueprint(name=u'plowed_tiles', terrain=True)
field_north_end_blueprint = Model_Blueprint(name=u'plowed_tiles_north_end', terrain=True)
field_south_end_blueprint = Model_Blueprint(name=u'plowed_tiles_south_end', terrain=True)

# model blueprints with collision solids:

bowl_blueprint = Model_Blueprint(name=u'bowl_test_02', collision_solids_generator=lambda: [CollisionSphere(0, 0, 0.2, 0.3)], icon='bowl')

hut_blueprint = Model_Blueprint(name=u'bowl_test_02', collision_solids_generator=lambda: [CollisionSphere(0, - 1.5, 0.5, 3)], scale=10, orientation=(0, 150, 0), height_offset=3, radius=3.5)

igloo_blueprint = Model_Blueprint(name=u'igloo', collision_solids_generator=lambda: [CollisionSphere(0, 0, 0.5, 1.5)], scale=0.15, radius=0.7)

fence_blueprint = Model_Blueprint(name=u'fence_01', collision_solids_generator=lambda: (CollisionPolygon(Vector3(- 2.06, i, 0.8 * __sign(i)), Vector3(2.06, i, 0.8 * __sign(i)), Vector3(2.06, i, - 0.8 * __sign(i)), Vector3(- 2.06, i, - 0.8 * __sign(i))) for i in (- 0.06, 0.06)), scale=0.615)

picket_fence_blueprint = Model_Blueprint(name=u'picket_fence', collision_solids_generator=lambda: (CollisionPolygon(Vector3(- 2.06, i, __sign(i)), Vector3(2.06, i, __sign(i)), Vector3(2.06, i, - __sign(i)), Vector3(- 2.06, i, - __sign(i))) for i in (- 0.06, 0.06)), scale=0.615, height_offset=1)

xemna_blueprint = Model_Blueprint(name=u'xemna_base_01', collision_solids_generator=lambda: [CollisionSphere(- 0.1, 0, 0, 0.2), CollisionSphere(0.1, 0, 0, 0.2)], scale=0.17)

# non-colliding model blueprints:

fruit_blueprint = Model_Blueprint(name=u'bowl_test_02', scale=0.2, orientation=(0, 180, 0), height_offset=0.09, icon='dagger')

pitcher_blueprint = Model_Blueprint(name=u'pitcher', icon='bowl')

clawhammer_blueprint = Model_Blueprint(name=u'clawhammer', orientation=(0, 0, 90), icon='clawhammer')

hatchet_blueprint = Model_Blueprint(name=u'hatchet', orientation=(0, 0, 90), icon='clawhammer')

heavyaxe_blueprint = Model_Blueprint(name=u'heavyaxe', orientation=(0, 0, 90))

hoe_blueprint = Model_Blueprint(name=u'hoe', orientation=(0, 0, 90), icon='clawhammer')

kama_blueprint = Model_Blueprint(name=u'kama', orientation=(0, 0, 90))

pickaxe_blueprint = Model_Blueprint(name=u'pickaxe', orientation=(0, 0, 90), icon='clawhammer')

pitchfork_blueprint = Model_Blueprint(name=u'pitchfork', orientation=(0, - 90, 90), icon='clawhammer')

scythe_blueprint = Model_Blueprint(name=u'scythe', orientation=(0, 0, 90), icon='clawhammer')

shovel_blueprint = Model_Blueprint(name=u'shovel', orientation=(0, 90, 90), icon='clawhammer')

sickle_blueprint = Model_Blueprint(name=u'sickle', orientation=(0, 0, 90), icon='clawhammer')

sledgehammer_blueprint = Model_Blueprint(name=u'sledgehammer', orientation=(0, 0, 90), icon='clawhammer')

trumpet_blueprint = Model_Blueprint(name=u'trumpet', scale=0.3, orientation=(0, 270, 0), height_offset=0.1, icon='trumpet')

knife_blueprint = Model_Blueprint(name=u'rusted_knife', icon='rusted_knife', scale=0.1)

__world = None

def create_world():

    global __world

    create_fixed_models()

    __world = World()

    feargus = Feargus(model=Model(blueprint=xemna_blueprint, coordinates=Vector3(5, 9, 0), horizontal_angle=- 30, scale=1.07), nutrition_value=consumable, name=u'Feargus', gender=male, race=xemna, description=u'Feargus is large, but ugly xemna.', container=__world)

    Container(capacity=50, name=u'%s backpack' % possessive(feargus), size=30, container=feargus.back, weight=3)

    kamdaz = Kamdaz(model=Model(blueprint=xemna_blueprint, coordinates=Vector3(- 10, 2, 0), horizontal_angle=210, scale=0.93), nutrition_value=consumable, name=u'Kamdaz', gender=male, description=u'Kamdaz is a frail looking xemna.', race=xemna, container=__world)

    Container(capacity=50, name=u'%s backpack' % possessive(kamdaz), weight=3, size=30, container=feargus.back)

    Item(model=Model(blueprint=sledgehammer_blueprint, coordinates=Vector3(6, 15, 0)),  nutrition_value=1000, name=u'a sledgehammer', size=1, container=__world, weight=7)

    Container(model=Model(blueprint=hut_blueprint, coordinates=Vector3(14, - 6, 0), horizontal_angle=225), capacity=10000, name=u'%s hut' % possessive(kamdaz), owner=kamdaz, size=15000, container=__world)

    Container(model=Model(blueprint=hut_blueprint, coordinates=Vector3(14, 2, 0), horizontal_angle=270), capacity=10000, name=u'%s hut' % possessive(u'Nobody'), size=15000, container=__world)

    feargus_hut = Container(model=Model(blueprint=hut_blueprint, coordinates=Vector3(12, 14, 0)), capacity=10000, name=u'%s hut' % possessive(feargus), owner=feargus, size=15000, container=__world)

    Item(description=u'That shiny dagger comes from %s hut.' % possessive(feargus), size=0.1, damage_modifier=5, name=u'a dagger', container=feargus_hut, weight=1)

    Container(model=Model(blueprint=bowl_blueprint, coordinates=Vector3(1, 1, 0)), capacity=5, description=u'That bowl comes from %s hut.' % possessive(feargus), size=5, name=u'a bowl', container=feargus_hut, weight=2)

    Item(model=Model(blueprint=sickle_blueprint, coordinates=Vector3(7, 14.5, 0)), damage_modifier=5, name=u'a sickle', size=1, container=__world, weight=3)

    Item(model=Model(blueprint=shovel_blueprint, coordinates=Vector3(7, 14, 0)), damage_modifier=5, name=u'a shovel', size=2, container=__world, weight=5)

    Item(model=Model(blueprint=scythe_blueprint, coordinates=Vector3(7, 13.5, 0)), damage_modifier=5, name=u'a scythe', size=3, container=__world, weight=7)

    Item(model=Model(blueprint=pitchfork_blueprint, coordinates=Vector3(7, 13, 0)), damage_modifier=5, name=u'a pitchfork', size=3, container=__world, weight=7)

    Item(model=Model(blueprint=pickaxe_blueprint, coordinates=Vector3(7, 12.5, 0)), damage_modifier=5, name=u'a pickaxe', size=3, container=__world, weight=7)

    Item(model=Model(blueprint=kama_blueprint, coordinates=Vector3(7, 12, 0)), damage_modifier=5, name=u'a kama', size=2, container=__world, weight=5)

    Item(model=Model(blueprint=hoe_blueprint, coordinates=Vector3(7, 11.5, 0)), damage_modifier=5, name=u'a hoe', size=2, container=__world, weight=5)

    Item(model=Model(blueprint=heavyaxe_blueprint, coordinates=Vector3(7, 11, 0)), damage_modifier=5, name=u'a heavyaxe', size=3, container=__world, weight=7)

    Item(model=Model(blueprint=hatchet_blueprint, coordinates=Vector3(7, 10.5, 0)), damage_modifier=5, name=u'a hatchet', size=1, container=__world, weight=5)

    Item(model=Model(blueprint=clawhammer_blueprint, coordinates=Vector3(7, 10, 0)), damage_modifier=5, name=u'a clawhammer', size=1, container=__world, weight=5)

    Item(model=Model(blueprint=fruit_blueprint, coordinates=Vector3(6, - 6, 0)), description=u'That peach looks delicious. Yum, yum.', name=u'a peach', container=__world, weight=0.2)

    Item(model=Model(blueprint=fruit_blueprint, coordinates=Vector3(- 5, 0, 0)), nutrition_value=consumable, name=u'an apple', weight=0.3, container=__world)

    Container(model=Model(blueprint=bowl_blueprint, coordinates=Vector3(- 2, - 4, 0)), capacity=5, size=5, name=u'a nice bowl', container=__world, weight=2)

    Container(model=Model(blueprint=pitcher_blueprint, coordinates=Vector3(1, - 1.5, 0)), capacity=5, size=5, name=u'a pitcher', container=__world, weight=2)

    Container(model=Model(blueprint=igloo_blueprint, coordinates=Vector3(4, - 6.5, 0)), capacity=1000, name=u'storage shack', container=__world)

    Item(model=Model(blueprint=trumpet_blueprint, coordinates=Vector3(40, 14, 0)), name=u'a conch trumpet', weight=4, container=__world, owner=kamdaz)

    Item(model=Model(blueprint=knife_blueprint, coordinates=Vector3(0, 0, 0)), damage_modifier=5, name=u'a rusted knife', weight=2, container=__world)

    for item in __world:

        item.model.start_collision()

def create_fixed_models():

    # the fields
    fields_group = Model_Group('fields')

    for i in [- 11, - 10, - 9, - 7.5, - 6.5, - 5.5, - 4, - 3, - 2]:

        Model(blueprint=field_north_end_blueprint, coordinates=Vector3(i, 15, 0), group=fields_group)

        for j in range(4, 15, 1):

            Model(blueprint=field_blueprint, coordinates=Vector3(i, j, 0), group=fields_group)

        Model(blueprint=field_south_end_blueprint, coordinates=Vector3(i, 3, 0), group=fields_group)

    fields_group.collect()

    # the fences
    # The plain fence models give problems when in the fences group.
    picket_fences_group = Model_Group('fences')

    for i in range(- 6, 15, 4):

        Model(blueprint=fence_blueprint, coordinates=Vector3(- 12, i, 0), horizontal_angle=270)

    Model(blueprint=picket_fence_blueprint, coordinates=Vector3(4, 10, 0), horizontal_angle=270, group=picket_fences_group)

    for i in (10, 14):

        Model(blueprint=picket_fence_blueprint, coordinates=Vector3(16, i, 0), horizontal_angle=90, group=picket_fences_group)

    for i in range(- 6, 7, 4):

        Model(blueprint=fence_blueprint, coordinates=Vector3(16, i, 0), horizontal_angle=90)

    for i in range(- 6, 15, 4):

        Model(blueprint=fence_blueprint, coordinates=Vector3(i, - 8, 0))

    for i in (6, 10, 14):

        Model(blueprint=picket_fence_blueprint, coordinates=Vector3(i, 8, 0), group=picket_fences_group)
        Model(blueprint=picket_fence_blueprint, coordinates=Vector3(i, 16, 0), horizontal_angle=180, group=picket_fences_group)

    for i in range(- 10, 3, 4):

        Model(blueprint=fence_blueprint, coordinates=Vector3(i, 16, 0), horizontal_angle=180)

    picket_fences_group.collect()

    # the ground
    Model(blueprint=ground_blueprint, coordinates=Vector3(0, 0, 0))

def create_player_character(name, gender, race, skill_levels):

    Player_Character(model=Model(blueprint=xemna_blueprint, coordinates=Vector3(- 10, - 12, 0), horizontal_angle=150), nutrition_value=consumable, name=name, skill_levels=skill_levels, gender=gender, race=race, container=__world)

    Container(capacity=50, name=u'%s backpack' % possessive(player_character), size=30, weight=3, container=player_character.back)
