#!/usr/bin/env python2
# -*- coding: utf-8 -*-

'''
Usage: 'radakan.py [applications]'

[applications] may consist of: 'an', 'doc', 'client', and 'test'.
'''

from sys import version_info

assert (version_info[0] == 2) and (6 <= version_info[1]), u'Please use Python 2.6+.'

from sys import argv

from common.print_exception_details import print_exception_details

def execute():

    planned_modules = [argument for argument in argv if not 'radakan' in argument.lower()]
    if len(planned_modules) == 0:
        planned_modules = ['client']

    module_paths = {
        'an': 'tools.analyzer.analyzer',
        'doc': 'tools.documentor.documentor',
        'client': 'client.client_',
        'tester': 'tools.tester.tester'
    }

    for module_name in planned_modules:

        print 'Running module \'{module_name}\'...'.format(** vars())

        try:
            module = __import__(module_paths[module_name], fromlist=['execute'])
        except KeyError:
            print 'Module \'%s\' couldn\'t be recognized.' % module_name
        else:

            try:
                module.execute()

            except Exception, exception:
                print_exception_details(exception, 'The \'%s\' module execution in \'radakan.py\'' % module_name)
                break

            except SystemExit:
                pass

            del module

            print 'Running module \'{module_name}\' completed.'.format(** vars())

# We might be in a Panda3D package, which has another main python file, so the standard check '__name__ == "__main__"' might not return the expected result.
execute()
